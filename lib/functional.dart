R? maybe<T, R>(T? element, R Function(T) consumer) => element == null ? null : consumer(element);
