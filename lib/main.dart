import 'package:flutter/material.dart';
import 'package:gitlab_dashboard/routes/common/objects.dart';
import 'package:gitlab_dashboard/routes/router.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:hive_flutter/hive_flutter.dart';

void main() async {
  Hive.registerAdapter(GitLabConfigAdapter());
  await Hive.initFlutter();
  await Hive.openBox<GitLabConfig>("gitlab-config");

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final _appRouter = AppRouter();
  final _client = _buildClientNotifier(Hive.box<GitLabConfig>("gitlab-config"), "default");

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) => MaterialApp.router(
        title: "GitLab Dashboard",
        theme: ThemeData.light(useMaterial3: true),
        darkTheme: ThemeData.dark(useMaterial3: true),
        themeMode: ThemeMode.system,
        builder: (context, child) => GraphQLProvider(
          client: ValueNotifier(_client),
          child: child,
        ),
        routerDelegate: _appRouter.delegate(),
        routeInformationParser: _appRouter.defaultRouteParser(),
      );

  static GraphQLClient _buildClientNotifier(Box<GitLabConfig> box, String name) {
    final httpEndpoint = HttpLink(box.get(name)?.endpoint ?? "https://gitlab.com/api/graphql");
    final authLink = AuthLink(getToken: () {
      final token = box.get(name)?.token ?? "";
      return token != "" ? "Bearer $token" : null;
    });

    final link = authLink.concat(httpEndpoint);

    return GraphQLClient(link: link, cache: GraphQLCache());
  }
}
