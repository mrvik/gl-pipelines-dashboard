import 'package:gql/ast.dart';

class Fragment$GroupInfo {
  Fragment$GroupInfo({
    required this.name,
    required this.fullPath,
    this.$__typename = 'Group',
  });

  factory Fragment$GroupInfo.fromJson(Map<String, dynamic> json) {
    final l$name = json['name'];
    final l$fullPath = json['fullPath'];
    final l$$__typename = json['__typename'];
    return Fragment$GroupInfo(
      name: (l$name as String),
      fullPath: (l$fullPath as String),
      $__typename: (l$$__typename as String),
    );
  }

  final String name;

  final String fullPath;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$name = name;
    _resultData['name'] = l$name;
    final l$fullPath = fullPath;
    _resultData['fullPath'] = l$fullPath;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$name = name;
    final l$fullPath = fullPath;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$name,
      l$fullPath,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$GroupInfo) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$name = name;
    final lOther$name = other.name;
    if (l$name != lOther$name) {
      return false;
    }
    final l$fullPath = fullPath;
    final lOther$fullPath = other.fullPath;
    if (l$fullPath != lOther$fullPath) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$GroupInfo on Fragment$GroupInfo {
  CopyWith$Fragment$GroupInfo<Fragment$GroupInfo> get copyWith =>
      CopyWith$Fragment$GroupInfo(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Fragment$GroupInfo<TRes> {
  factory CopyWith$Fragment$GroupInfo(
    Fragment$GroupInfo instance,
    TRes Function(Fragment$GroupInfo) then,
  ) = _CopyWithImpl$Fragment$GroupInfo;

  factory CopyWith$Fragment$GroupInfo.stub(TRes res) =
      _CopyWithStubImpl$Fragment$GroupInfo;

  TRes call({
    String? name,
    String? fullPath,
    String? $__typename,
  });
}

class _CopyWithImpl$Fragment$GroupInfo<TRes>
    implements CopyWith$Fragment$GroupInfo<TRes> {
  _CopyWithImpl$Fragment$GroupInfo(
    this._instance,
    this._then,
  );

  final Fragment$GroupInfo _instance;

  final TRes Function(Fragment$GroupInfo) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? name = _undefined,
    Object? fullPath = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$GroupInfo(
        name: name == _undefined || name == null
            ? _instance.name
            : (name as String),
        fullPath: fullPath == _undefined || fullPath == null
            ? _instance.fullPath
            : (fullPath as String),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Fragment$GroupInfo<TRes>
    implements CopyWith$Fragment$GroupInfo<TRes> {
  _CopyWithStubImpl$Fragment$GroupInfo(this._res);

  TRes _res;

  call({
    String? name,
    String? fullPath,
    String? $__typename,
  }) =>
      _res;
}

const fragmentDefinitionGroupInfo = FragmentDefinitionNode(
  name: NameNode(value: 'GroupInfo'),
  typeCondition: TypeConditionNode(
      on: NamedTypeNode(
    name: NameNode(value: 'Group'),
    isNonNull: false,
  )),
  directives: [],
  selectionSet: SelectionSetNode(selections: [
    FieldNode(
      name: NameNode(value: 'name'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'fullPath'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: '__typename'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
  ]),
);
const documentNodeFragmentGroupInfo = DocumentNode(definitions: [
  fragmentDefinitionGroupInfo,
]);

class Query$UserGroups {
  Query$UserGroups({
    this.currentUser,
    this.$__typename = 'Query',
  });

  factory Query$UserGroups.fromJson(Map<String, dynamic> json) {
    final l$currentUser = json['currentUser'];
    final l$$__typename = json['__typename'];
    return Query$UserGroups(
      currentUser: l$currentUser == null
          ? null
          : Query$UserGroups$currentUser.fromJson(
              (l$currentUser as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Query$UserGroups$currentUser? currentUser;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$currentUser = currentUser;
    _resultData['currentUser'] = l$currentUser?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$currentUser = currentUser;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$currentUser,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$UserGroups) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$currentUser = currentUser;
    final lOther$currentUser = other.currentUser;
    if (l$currentUser != lOther$currentUser) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$UserGroups on Query$UserGroups {
  CopyWith$Query$UserGroups<Query$UserGroups> get copyWith =>
      CopyWith$Query$UserGroups(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Query$UserGroups<TRes> {
  factory CopyWith$Query$UserGroups(
    Query$UserGroups instance,
    TRes Function(Query$UserGroups) then,
  ) = _CopyWithImpl$Query$UserGroups;

  factory CopyWith$Query$UserGroups.stub(TRes res) =
      _CopyWithStubImpl$Query$UserGroups;

  TRes call({
    Query$UserGroups$currentUser? currentUser,
    String? $__typename,
  });
  CopyWith$Query$UserGroups$currentUser<TRes> get currentUser;
}

class _CopyWithImpl$Query$UserGroups<TRes>
    implements CopyWith$Query$UserGroups<TRes> {
  _CopyWithImpl$Query$UserGroups(
    this._instance,
    this._then,
  );

  final Query$UserGroups _instance;

  final TRes Function(Query$UserGroups) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? currentUser = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$UserGroups(
        currentUser: currentUser == _undefined
            ? _instance.currentUser
            : (currentUser as Query$UserGroups$currentUser?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Query$UserGroups$currentUser<TRes> get currentUser {
    final local$currentUser = _instance.currentUser;
    return local$currentUser == null
        ? CopyWith$Query$UserGroups$currentUser.stub(_then(_instance))
        : CopyWith$Query$UserGroups$currentUser(
            local$currentUser, (e) => call(currentUser: e));
  }
}

class _CopyWithStubImpl$Query$UserGroups<TRes>
    implements CopyWith$Query$UserGroups<TRes> {
  _CopyWithStubImpl$Query$UserGroups(this._res);

  TRes _res;

  call({
    Query$UserGroups$currentUser? currentUser,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Query$UserGroups$currentUser<TRes> get currentUser =>
      CopyWith$Query$UserGroups$currentUser.stub(_res);
}

const documentNodeQueryUserGroups = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.query,
    name: NameNode(value: 'UserGroups'),
    variableDefinitions: [],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'currentUser'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
            name: NameNode(value: 'groups'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                name: NameNode(value: 'nodes'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: SelectionSetNode(selections: [
                  FragmentSpreadNode(
                    name: NameNode(value: 'GroupInfo'),
                    directives: [],
                  ),
                  FieldNode(
                    name: NameNode(value: '__typename'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null,
                  ),
                ]),
              ),
              FieldNode(
                name: NameNode(value: '__typename'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
            ]),
          ),
          FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
        ]),
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
  fragmentDefinitionGroupInfo,
]);

class Query$UserGroups$currentUser {
  Query$UserGroups$currentUser({
    this.groups,
    this.$__typename = 'CurrentUser',
  });

  factory Query$UserGroups$currentUser.fromJson(Map<String, dynamic> json) {
    final l$groups = json['groups'];
    final l$$__typename = json['__typename'];
    return Query$UserGroups$currentUser(
      groups: l$groups == null
          ? null
          : Query$UserGroups$currentUser$groups.fromJson(
              (l$groups as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Query$UserGroups$currentUser$groups? groups;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$groups = groups;
    _resultData['groups'] = l$groups?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$groups = groups;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$groups,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$UserGroups$currentUser) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$groups = groups;
    final lOther$groups = other.groups;
    if (l$groups != lOther$groups) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$UserGroups$currentUser
    on Query$UserGroups$currentUser {
  CopyWith$Query$UserGroups$currentUser<Query$UserGroups$currentUser>
      get copyWith => CopyWith$Query$UserGroups$currentUser(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$UserGroups$currentUser<TRes> {
  factory CopyWith$Query$UserGroups$currentUser(
    Query$UserGroups$currentUser instance,
    TRes Function(Query$UserGroups$currentUser) then,
  ) = _CopyWithImpl$Query$UserGroups$currentUser;

  factory CopyWith$Query$UserGroups$currentUser.stub(TRes res) =
      _CopyWithStubImpl$Query$UserGroups$currentUser;

  TRes call({
    Query$UserGroups$currentUser$groups? groups,
    String? $__typename,
  });
  CopyWith$Query$UserGroups$currentUser$groups<TRes> get groups;
}

class _CopyWithImpl$Query$UserGroups$currentUser<TRes>
    implements CopyWith$Query$UserGroups$currentUser<TRes> {
  _CopyWithImpl$Query$UserGroups$currentUser(
    this._instance,
    this._then,
  );

  final Query$UserGroups$currentUser _instance;

  final TRes Function(Query$UserGroups$currentUser) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? groups = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$UserGroups$currentUser(
        groups: groups == _undefined
            ? _instance.groups
            : (groups as Query$UserGroups$currentUser$groups?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Query$UserGroups$currentUser$groups<TRes> get groups {
    final local$groups = _instance.groups;
    return local$groups == null
        ? CopyWith$Query$UserGroups$currentUser$groups.stub(_then(_instance))
        : CopyWith$Query$UserGroups$currentUser$groups(
            local$groups, (e) => call(groups: e));
  }
}

class _CopyWithStubImpl$Query$UserGroups$currentUser<TRes>
    implements CopyWith$Query$UserGroups$currentUser<TRes> {
  _CopyWithStubImpl$Query$UserGroups$currentUser(this._res);

  TRes _res;

  call({
    Query$UserGroups$currentUser$groups? groups,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Query$UserGroups$currentUser$groups<TRes> get groups =>
      CopyWith$Query$UserGroups$currentUser$groups.stub(_res);
}

class Query$UserGroups$currentUser$groups {
  Query$UserGroups$currentUser$groups({
    this.nodes,
    this.$__typename = 'GroupConnection',
  });

  factory Query$UserGroups$currentUser$groups.fromJson(
      Map<String, dynamic> json) {
    final l$nodes = json['nodes'];
    final l$$__typename = json['__typename'];
    return Query$UserGroups$currentUser$groups(
      nodes: (l$nodes as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Fragment$GroupInfo.fromJson((e as Map<String, dynamic>)))
          .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final List<Fragment$GroupInfo?>? nodes;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$nodes = nodes;
    _resultData['nodes'] = l$nodes?.map((e) => e?.toJson()).toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$nodes = nodes;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$nodes == null ? null : Object.hashAll(l$nodes.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$UserGroups$currentUser$groups) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$nodes = nodes;
    final lOther$nodes = other.nodes;
    if (l$nodes != null && lOther$nodes != null) {
      if (l$nodes.length != lOther$nodes.length) {
        return false;
      }
      for (int i = 0; i < l$nodes.length; i++) {
        final l$nodes$entry = l$nodes[i];
        final lOther$nodes$entry = lOther$nodes[i];
        if (l$nodes$entry != lOther$nodes$entry) {
          return false;
        }
      }
    } else if (l$nodes != lOther$nodes) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$UserGroups$currentUser$groups
    on Query$UserGroups$currentUser$groups {
  CopyWith$Query$UserGroups$currentUser$groups<
          Query$UserGroups$currentUser$groups>
      get copyWith => CopyWith$Query$UserGroups$currentUser$groups(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$UserGroups$currentUser$groups<TRes> {
  factory CopyWith$Query$UserGroups$currentUser$groups(
    Query$UserGroups$currentUser$groups instance,
    TRes Function(Query$UserGroups$currentUser$groups) then,
  ) = _CopyWithImpl$Query$UserGroups$currentUser$groups;

  factory CopyWith$Query$UserGroups$currentUser$groups.stub(TRes res) =
      _CopyWithStubImpl$Query$UserGroups$currentUser$groups;

  TRes call({
    List<Fragment$GroupInfo?>? nodes,
    String? $__typename,
  });
  TRes nodes(
      Iterable<Fragment$GroupInfo?>? Function(
              Iterable<CopyWith$Fragment$GroupInfo<Fragment$GroupInfo>?>?)
          _fn);
}

class _CopyWithImpl$Query$UserGroups$currentUser$groups<TRes>
    implements CopyWith$Query$UserGroups$currentUser$groups<TRes> {
  _CopyWithImpl$Query$UserGroups$currentUser$groups(
    this._instance,
    this._then,
  );

  final Query$UserGroups$currentUser$groups _instance;

  final TRes Function(Query$UserGroups$currentUser$groups) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? nodes = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$UserGroups$currentUser$groups(
        nodes: nodes == _undefined
            ? _instance.nodes
            : (nodes as List<Fragment$GroupInfo?>?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  TRes nodes(
          Iterable<Fragment$GroupInfo?>? Function(
                  Iterable<CopyWith$Fragment$GroupInfo<Fragment$GroupInfo>?>?)
              _fn) =>
      call(
          nodes: _fn(_instance.nodes?.map((e) => e == null
              ? null
              : CopyWith$Fragment$GroupInfo(
                  e,
                  (i) => i,
                )))?.toList());
}

class _CopyWithStubImpl$Query$UserGroups$currentUser$groups<TRes>
    implements CopyWith$Query$UserGroups$currentUser$groups<TRes> {
  _CopyWithStubImpl$Query$UserGroups$currentUser$groups(this._res);

  TRes _res;

  call({
    List<Fragment$GroupInfo?>? nodes,
    String? $__typename,
  }) =>
      _res;

  nodes(_fn) => _res;
}
