import 'package:gql/ast.dart';
import 'schema.graphql.dart';

class Fragment$UserProjects {
  Fragment$UserProjects({
    this.projectMemberships,
    this.$__typename = 'CurrentUser',
  });

  factory Fragment$UserProjects.fromJson(Map<String, dynamic> json) {
    final l$projectMemberships = json['projectMemberships'];
    final l$$__typename = json['__typename'];
    return Fragment$UserProjects(
      projectMemberships: l$projectMemberships == null
          ? null
          : Fragment$UserProjects$projectMemberships.fromJson(
              (l$projectMemberships as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Fragment$UserProjects$projectMemberships? projectMemberships;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$projectMemberships = projectMemberships;
    _resultData['projectMemberships'] = l$projectMemberships?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$projectMemberships = projectMemberships;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$projectMemberships,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$UserProjects) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$projectMemberships = projectMemberships;
    final lOther$projectMemberships = other.projectMemberships;
    if (l$projectMemberships != lOther$projectMemberships) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$UserProjects on Fragment$UserProjects {
  CopyWith$Fragment$UserProjects<Fragment$UserProjects> get copyWith =>
      CopyWith$Fragment$UserProjects(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Fragment$UserProjects<TRes> {
  factory CopyWith$Fragment$UserProjects(
    Fragment$UserProjects instance,
    TRes Function(Fragment$UserProjects) then,
  ) = _CopyWithImpl$Fragment$UserProjects;

  factory CopyWith$Fragment$UserProjects.stub(TRes res) =
      _CopyWithStubImpl$Fragment$UserProjects;

  TRes call({
    Fragment$UserProjects$projectMemberships? projectMemberships,
    String? $__typename,
  });
  CopyWith$Fragment$UserProjects$projectMemberships<TRes>
      get projectMemberships;
}

class _CopyWithImpl$Fragment$UserProjects<TRes>
    implements CopyWith$Fragment$UserProjects<TRes> {
  _CopyWithImpl$Fragment$UserProjects(
    this._instance,
    this._then,
  );

  final Fragment$UserProjects _instance;

  final TRes Function(Fragment$UserProjects) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? projectMemberships = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$UserProjects(
        projectMemberships: projectMemberships == _undefined
            ? _instance.projectMemberships
            : (projectMemberships as Fragment$UserProjects$projectMemberships?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Fragment$UserProjects$projectMemberships<TRes>
      get projectMemberships {
    final local$projectMemberships = _instance.projectMemberships;
    return local$projectMemberships == null
        ? CopyWith$Fragment$UserProjects$projectMemberships.stub(
            _then(_instance))
        : CopyWith$Fragment$UserProjects$projectMemberships(
            local$projectMemberships, (e) => call(projectMemberships: e));
  }
}

class _CopyWithStubImpl$Fragment$UserProjects<TRes>
    implements CopyWith$Fragment$UserProjects<TRes> {
  _CopyWithStubImpl$Fragment$UserProjects(this._res);

  TRes _res;

  call({
    Fragment$UserProjects$projectMemberships? projectMemberships,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Fragment$UserProjects$projectMemberships<TRes>
      get projectMemberships =>
          CopyWith$Fragment$UserProjects$projectMemberships.stub(_res);
}

const fragmentDefinitionUserProjects = FragmentDefinitionNode(
  name: NameNode(value: 'UserProjects'),
  typeCondition: TypeConditionNode(
      on: NamedTypeNode(
    name: NameNode(value: 'CurrentUser'),
    isNonNull: false,
  )),
  directives: [],
  selectionSet: SelectionSetNode(selections: [
    FieldNode(
      name: NameNode(value: 'projectMemberships'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
          name: NameNode(value: 'nodes'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: SelectionSetNode(selections: [
            FieldNode(
              name: NameNode(value: 'project'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                  name: NameNode(value: 'group'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                      name: NameNode(value: 'name'),
                      alias: null,
                      arguments: [],
                      directives: [],
                      selectionSet: null,
                    ),
                    FieldNode(
                      name: NameNode(value: '__typename'),
                      alias: null,
                      arguments: [],
                      directives: [],
                      selectionSet: null,
                    ),
                  ]),
                ),
                FieldNode(
                  name: NameNode(value: 'namespace'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                      name: NameNode(value: 'fullPath'),
                      alias: null,
                      arguments: [],
                      directives: [],
                      selectionSet: null,
                    ),
                    FieldNode(
                      name: NameNode(value: '__typename'),
                      alias: null,
                      arguments: [],
                      directives: [],
                      selectionSet: null,
                    ),
                  ]),
                ),
                FragmentSpreadNode(
                  name: NameNode(value: 'ProjectFields'),
                  directives: [],
                ),
                FieldNode(
                  name: NameNode(value: '__typename'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null,
                ),
              ]),
            ),
            FieldNode(
              name: NameNode(value: '__typename'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: null,
            ),
          ]),
        ),
        FieldNode(
          name: NameNode(value: '__typename'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
      ]),
    ),
    FieldNode(
      name: NameNode(value: '__typename'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
  ]),
);
const documentNodeFragmentUserProjects = DocumentNode(definitions: [
  fragmentDefinitionUserProjects,
  fragmentDefinitionProjectFields,
  fragmentDefinitionPipelineFields,
]);

class Fragment$UserProjects$projectMemberships {
  Fragment$UserProjects$projectMemberships({
    this.nodes,
    this.$__typename = 'ProjectMemberConnection',
  });

  factory Fragment$UserProjects$projectMemberships.fromJson(
      Map<String, dynamic> json) {
    final l$nodes = json['nodes'];
    final l$$__typename = json['__typename'];
    return Fragment$UserProjects$projectMemberships(
      nodes: (l$nodes as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Fragment$UserProjects$projectMemberships$nodes.fromJson(
                  (e as Map<String, dynamic>)))
          .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final List<Fragment$UserProjects$projectMemberships$nodes?>? nodes;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$nodes = nodes;
    _resultData['nodes'] = l$nodes?.map((e) => e?.toJson()).toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$nodes = nodes;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$nodes == null ? null : Object.hashAll(l$nodes.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$UserProjects$projectMemberships) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$nodes = nodes;
    final lOther$nodes = other.nodes;
    if (l$nodes != null && lOther$nodes != null) {
      if (l$nodes.length != lOther$nodes.length) {
        return false;
      }
      for (int i = 0; i < l$nodes.length; i++) {
        final l$nodes$entry = l$nodes[i];
        final lOther$nodes$entry = lOther$nodes[i];
        if (l$nodes$entry != lOther$nodes$entry) {
          return false;
        }
      }
    } else if (l$nodes != lOther$nodes) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$UserProjects$projectMemberships
    on Fragment$UserProjects$projectMemberships {
  CopyWith$Fragment$UserProjects$projectMemberships<
          Fragment$UserProjects$projectMemberships>
      get copyWith => CopyWith$Fragment$UserProjects$projectMemberships(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$UserProjects$projectMemberships<TRes> {
  factory CopyWith$Fragment$UserProjects$projectMemberships(
    Fragment$UserProjects$projectMemberships instance,
    TRes Function(Fragment$UserProjects$projectMemberships) then,
  ) = _CopyWithImpl$Fragment$UserProjects$projectMemberships;

  factory CopyWith$Fragment$UserProjects$projectMemberships.stub(TRes res) =
      _CopyWithStubImpl$Fragment$UserProjects$projectMemberships;

  TRes call({
    List<Fragment$UserProjects$projectMemberships$nodes?>? nodes,
    String? $__typename,
  });
  TRes nodes(
      Iterable<Fragment$UserProjects$projectMemberships$nodes?>? Function(
              Iterable<
                  CopyWith$Fragment$UserProjects$projectMemberships$nodes<
                      Fragment$UserProjects$projectMemberships$nodes>?>?)
          _fn);
}

class _CopyWithImpl$Fragment$UserProjects$projectMemberships<TRes>
    implements CopyWith$Fragment$UserProjects$projectMemberships<TRes> {
  _CopyWithImpl$Fragment$UserProjects$projectMemberships(
    this._instance,
    this._then,
  );

  final Fragment$UserProjects$projectMemberships _instance;

  final TRes Function(Fragment$UserProjects$projectMemberships) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? nodes = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$UserProjects$projectMemberships(
        nodes: nodes == _undefined
            ? _instance.nodes
            : (nodes as List<Fragment$UserProjects$projectMemberships$nodes?>?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  TRes nodes(
          Iterable<Fragment$UserProjects$projectMemberships$nodes?>? Function(
                  Iterable<
                      CopyWith$Fragment$UserProjects$projectMemberships$nodes<
                          Fragment$UserProjects$projectMemberships$nodes>?>?)
              _fn) =>
      call(
          nodes: _fn(_instance.nodes?.map((e) => e == null
              ? null
              : CopyWith$Fragment$UserProjects$projectMemberships$nodes(
                  e,
                  (i) => i,
                )))?.toList());
}

class _CopyWithStubImpl$Fragment$UserProjects$projectMemberships<TRes>
    implements CopyWith$Fragment$UserProjects$projectMemberships<TRes> {
  _CopyWithStubImpl$Fragment$UserProjects$projectMemberships(this._res);

  TRes _res;

  call({
    List<Fragment$UserProjects$projectMemberships$nodes?>? nodes,
    String? $__typename,
  }) =>
      _res;

  nodes(_fn) => _res;
}

class Fragment$UserProjects$projectMemberships$nodes {
  Fragment$UserProjects$projectMemberships$nodes({
    this.project,
    this.$__typename = 'ProjectMember',
  });

  factory Fragment$UserProjects$projectMemberships$nodes.fromJson(
      Map<String, dynamic> json) {
    final l$project = json['project'];
    final l$$__typename = json['__typename'];
    return Fragment$UserProjects$projectMemberships$nodes(
      project: l$project == null
          ? null
          : Fragment$UserProjects$projectMemberships$nodes$project.fromJson(
              (l$project as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Fragment$UserProjects$projectMemberships$nodes$project? project;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$project = project;
    _resultData['project'] = l$project?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$project = project;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$project,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$UserProjects$projectMemberships$nodes) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$project = project;
    final lOther$project = other.project;
    if (l$project != lOther$project) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$UserProjects$projectMemberships$nodes
    on Fragment$UserProjects$projectMemberships$nodes {
  CopyWith$Fragment$UserProjects$projectMemberships$nodes<
          Fragment$UserProjects$projectMemberships$nodes>
      get copyWith => CopyWith$Fragment$UserProjects$projectMemberships$nodes(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$UserProjects$projectMemberships$nodes<TRes> {
  factory CopyWith$Fragment$UserProjects$projectMemberships$nodes(
    Fragment$UserProjects$projectMemberships$nodes instance,
    TRes Function(Fragment$UserProjects$projectMemberships$nodes) then,
  ) = _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes;

  factory CopyWith$Fragment$UserProjects$projectMemberships$nodes.stub(
          TRes res) =
      _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes;

  TRes call({
    Fragment$UserProjects$projectMemberships$nodes$project? project,
    String? $__typename,
  });
  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project<TRes>
      get project;
}

class _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes<TRes>
    implements CopyWith$Fragment$UserProjects$projectMemberships$nodes<TRes> {
  _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes(
    this._instance,
    this._then,
  );

  final Fragment$UserProjects$projectMemberships$nodes _instance;

  final TRes Function(Fragment$UserProjects$projectMemberships$nodes) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? project = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$UserProjects$projectMemberships$nodes(
        project: project == _undefined
            ? _instance.project
            : (project
                as Fragment$UserProjects$projectMemberships$nodes$project?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project<TRes>
      get project {
    final local$project = _instance.project;
    return local$project == null
        ? CopyWith$Fragment$UserProjects$projectMemberships$nodes$project.stub(
            _then(_instance))
        : CopyWith$Fragment$UserProjects$projectMemberships$nodes$project(
            local$project, (e) => call(project: e));
  }
}

class _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes<TRes>
    implements CopyWith$Fragment$UserProjects$projectMemberships$nodes<TRes> {
  _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes(this._res);

  TRes _res;

  call({
    Fragment$UserProjects$projectMemberships$nodes$project? project,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project<TRes>
      get project =>
          CopyWith$Fragment$UserProjects$projectMemberships$nodes$project.stub(
              _res);
}

class Fragment$UserProjects$projectMemberships$nodes$project
    implements Fragment$ProjectFields {
  Fragment$UserProjects$projectMemberships$nodes$project({
    this.group,
    this.namespace,
    required this.name,
    required this.fullPath,
    this.repository,
    this.pipelines,
    this.tagPipelines,
    this.$__typename = 'Project',
  });

  factory Fragment$UserProjects$projectMemberships$nodes$project.fromJson(
      Map<String, dynamic> json) {
    final l$group = json['group'];
    final l$namespace = json['namespace'];
    final l$name = json['name'];
    final l$fullPath = json['fullPath'];
    final l$repository = json['repository'];
    final l$pipelines = json['pipelines'];
    final l$tagPipelines = json['tagPipelines'];
    final l$$__typename = json['__typename'];
    return Fragment$UserProjects$projectMemberships$nodes$project(
      group: l$group == null
          ? null
          : Fragment$UserProjects$projectMemberships$nodes$project$group
              .fromJson((l$group as Map<String, dynamic>)),
      namespace: l$namespace == null
          ? null
          : Fragment$UserProjects$projectMemberships$nodes$project$namespace
              .fromJson((l$namespace as Map<String, dynamic>)),
      name: (l$name as String),
      fullPath: (l$fullPath as String),
      repository: l$repository == null
          ? null
          : Fragment$UserProjects$projectMemberships$nodes$project$repository
              .fromJson((l$repository as Map<String, dynamic>)),
      pipelines: l$pipelines == null
          ? null
          : Fragment$UserProjects$projectMemberships$nodes$project$pipelines
              .fromJson((l$pipelines as Map<String, dynamic>)),
      tagPipelines: l$tagPipelines == null
          ? null
          : Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines
              .fromJson((l$tagPipelines as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Fragment$UserProjects$projectMemberships$nodes$project$group? group;

  final Fragment$UserProjects$projectMemberships$nodes$project$namespace?
      namespace;

  final String name;

  final String fullPath;

  final Fragment$UserProjects$projectMemberships$nodes$project$repository?
      repository;

  final Fragment$UserProjects$projectMemberships$nodes$project$pipelines?
      pipelines;

  final Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines?
      tagPipelines;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$group = group;
    _resultData['group'] = l$group?.toJson();
    final l$namespace = namespace;
    _resultData['namespace'] = l$namespace?.toJson();
    final l$name = name;
    _resultData['name'] = l$name;
    final l$fullPath = fullPath;
    _resultData['fullPath'] = l$fullPath;
    final l$repository = repository;
    _resultData['repository'] = l$repository?.toJson();
    final l$pipelines = pipelines;
    _resultData['pipelines'] = l$pipelines?.toJson();
    final l$tagPipelines = tagPipelines;
    _resultData['tagPipelines'] = l$tagPipelines?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$group = group;
    final l$namespace = namespace;
    final l$name = name;
    final l$fullPath = fullPath;
    final l$repository = repository;
    final l$pipelines = pipelines;
    final l$tagPipelines = tagPipelines;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$group,
      l$namespace,
      l$name,
      l$fullPath,
      l$repository,
      l$pipelines,
      l$tagPipelines,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$UserProjects$projectMemberships$nodes$project) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$group = group;
    final lOther$group = other.group;
    if (l$group != lOther$group) {
      return false;
    }
    final l$namespace = namespace;
    final lOther$namespace = other.namespace;
    if (l$namespace != lOther$namespace) {
      return false;
    }
    final l$name = name;
    final lOther$name = other.name;
    if (l$name != lOther$name) {
      return false;
    }
    final l$fullPath = fullPath;
    final lOther$fullPath = other.fullPath;
    if (l$fullPath != lOther$fullPath) {
      return false;
    }
    final l$repository = repository;
    final lOther$repository = other.repository;
    if (l$repository != lOther$repository) {
      return false;
    }
    final l$pipelines = pipelines;
    final lOther$pipelines = other.pipelines;
    if (l$pipelines != lOther$pipelines) {
      return false;
    }
    final l$tagPipelines = tagPipelines;
    final lOther$tagPipelines = other.tagPipelines;
    if (l$tagPipelines != lOther$tagPipelines) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$UserProjects$projectMemberships$nodes$project
    on Fragment$UserProjects$projectMemberships$nodes$project {
  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project<
          Fragment$UserProjects$projectMemberships$nodes$project>
      get copyWith =>
          CopyWith$Fragment$UserProjects$projectMemberships$nodes$project(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$UserProjects$projectMemberships$nodes$project<
    TRes> {
  factory CopyWith$Fragment$UserProjects$projectMemberships$nodes$project(
    Fragment$UserProjects$projectMemberships$nodes$project instance,
    TRes Function(Fragment$UserProjects$projectMemberships$nodes$project) then,
  ) = _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project;

  factory CopyWith$Fragment$UserProjects$projectMemberships$nodes$project.stub(
          TRes res) =
      _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project;

  TRes call({
    Fragment$UserProjects$projectMemberships$nodes$project$group? group,
    Fragment$UserProjects$projectMemberships$nodes$project$namespace? namespace,
    String? name,
    String? fullPath,
    Fragment$UserProjects$projectMemberships$nodes$project$repository?
        repository,
    Fragment$UserProjects$projectMemberships$nodes$project$pipelines? pipelines,
    Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines?
        tagPipelines,
    String? $__typename,
  });
  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$group<TRes>
      get group;
  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$namespace<
      TRes> get namespace;
  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$repository<
      TRes> get repository;
  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$pipelines<
      TRes> get pipelines;
  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines<
      TRes> get tagPipelines;
}

class _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project<TRes>
    implements
        CopyWith$Fragment$UserProjects$projectMemberships$nodes$project<TRes> {
  _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project(
    this._instance,
    this._then,
  );

  final Fragment$UserProjects$projectMemberships$nodes$project _instance;

  final TRes Function(Fragment$UserProjects$projectMemberships$nodes$project)
      _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? group = _undefined,
    Object? namespace = _undefined,
    Object? name = _undefined,
    Object? fullPath = _undefined,
    Object? repository = _undefined,
    Object? pipelines = _undefined,
    Object? tagPipelines = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$UserProjects$projectMemberships$nodes$project(
        group: group == _undefined
            ? _instance.group
            : (group
                as Fragment$UserProjects$projectMemberships$nodes$project$group?),
        namespace: namespace == _undefined
            ? _instance.namespace
            : (namespace
                as Fragment$UserProjects$projectMemberships$nodes$project$namespace?),
        name: name == _undefined || name == null
            ? _instance.name
            : (name as String),
        fullPath: fullPath == _undefined || fullPath == null
            ? _instance.fullPath
            : (fullPath as String),
        repository: repository == _undefined
            ? _instance.repository
            : (repository
                as Fragment$UserProjects$projectMemberships$nodes$project$repository?),
        pipelines: pipelines == _undefined
            ? _instance.pipelines
            : (pipelines
                as Fragment$UserProjects$projectMemberships$nodes$project$pipelines?),
        tagPipelines: tagPipelines == _undefined
            ? _instance.tagPipelines
            : (tagPipelines
                as Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$group<TRes>
      get group {
    final local$group = _instance.group;
    return local$group == null
        ? CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$group
            .stub(_then(_instance))
        : CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$group(
            local$group, (e) => call(group: e));
  }

  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$namespace<
      TRes> get namespace {
    final local$namespace = _instance.namespace;
    return local$namespace == null
        ? CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$namespace
            .stub(_then(_instance))
        : CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$namespace(
            local$namespace, (e) => call(namespace: e));
  }

  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$repository<
      TRes> get repository {
    final local$repository = _instance.repository;
    return local$repository == null
        ? CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$repository
            .stub(_then(_instance))
        : CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$repository(
            local$repository, (e) => call(repository: e));
  }

  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$pipelines<
      TRes> get pipelines {
    final local$pipelines = _instance.pipelines;
    return local$pipelines == null
        ? CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$pipelines
            .stub(_then(_instance))
        : CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$pipelines(
            local$pipelines, (e) => call(pipelines: e));
  }

  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines<
      TRes> get tagPipelines {
    final local$tagPipelines = _instance.tagPipelines;
    return local$tagPipelines == null
        ? CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines
            .stub(_then(_instance))
        : CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines(
            local$tagPipelines, (e) => call(tagPipelines: e));
  }
}

class _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project<
        TRes>
    implements
        CopyWith$Fragment$UserProjects$projectMemberships$nodes$project<TRes> {
  _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project(
      this._res);

  TRes _res;

  call({
    Fragment$UserProjects$projectMemberships$nodes$project$group? group,
    Fragment$UserProjects$projectMemberships$nodes$project$namespace? namespace,
    String? name,
    String? fullPath,
    Fragment$UserProjects$projectMemberships$nodes$project$repository?
        repository,
    Fragment$UserProjects$projectMemberships$nodes$project$pipelines? pipelines,
    Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines?
        tagPipelines,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$group<TRes>
      get group =>
          CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$group
              .stub(_res);

  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$namespace<
          TRes>
      get namespace =>
          CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$namespace
              .stub(_res);

  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$repository<
          TRes>
      get repository =>
          CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$repository
              .stub(_res);

  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$pipelines<
          TRes>
      get pipelines =>
          CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$pipelines
              .stub(_res);

  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines<
          TRes>
      get tagPipelines =>
          CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines
              .stub(_res);
}

class Fragment$UserProjects$projectMemberships$nodes$project$group {
  Fragment$UserProjects$projectMemberships$nodes$project$group({
    required this.name,
    this.$__typename = 'Group',
  });

  factory Fragment$UserProjects$projectMemberships$nodes$project$group.fromJson(
      Map<String, dynamic> json) {
    final l$name = json['name'];
    final l$$__typename = json['__typename'];
    return Fragment$UserProjects$projectMemberships$nodes$project$group(
      name: (l$name as String),
      $__typename: (l$$__typename as String),
    );
  }

  final String name;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$name = name;
    _resultData['name'] = l$name;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$name = name;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$name,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other
            is Fragment$UserProjects$projectMemberships$nodes$project$group) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$name = name;
    final lOther$name = other.name;
    if (l$name != lOther$name) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$UserProjects$projectMemberships$nodes$project$group
    on Fragment$UserProjects$projectMemberships$nodes$project$group {
  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$group<
          Fragment$UserProjects$projectMemberships$nodes$project$group>
      get copyWith =>
          CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$group(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$group<
    TRes> {
  factory CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$group(
    Fragment$UserProjects$projectMemberships$nodes$project$group instance,
    TRes Function(Fragment$UserProjects$projectMemberships$nodes$project$group)
        then,
  ) = _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$group;

  factory CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$group.stub(
          TRes res) =
      _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$group;

  TRes call({
    String? name,
    String? $__typename,
  });
}

class _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$group<
        TRes>
    implements
        CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$group<
            TRes> {
  _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$group(
    this._instance,
    this._then,
  );

  final Fragment$UserProjects$projectMemberships$nodes$project$group _instance;

  final TRes Function(
      Fragment$UserProjects$projectMemberships$nodes$project$group) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? name = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$UserProjects$projectMemberships$nodes$project$group(
        name: name == _undefined || name == null
            ? _instance.name
            : (name as String),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$group<
        TRes>
    implements
        CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$group<
            TRes> {
  _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$group(
      this._res);

  TRes _res;

  call({
    String? name,
    String? $__typename,
  }) =>
      _res;
}

class Fragment$UserProjects$projectMemberships$nodes$project$namespace {
  Fragment$UserProjects$projectMemberships$nodes$project$namespace({
    required this.fullPath,
    this.$__typename = 'Namespace',
  });

  factory Fragment$UserProjects$projectMemberships$nodes$project$namespace.fromJson(
      Map<String, dynamic> json) {
    final l$fullPath = json['fullPath'];
    final l$$__typename = json['__typename'];
    return Fragment$UserProjects$projectMemberships$nodes$project$namespace(
      fullPath: (l$fullPath as String),
      $__typename: (l$$__typename as String),
    );
  }

  final String fullPath;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$fullPath = fullPath;
    _resultData['fullPath'] = l$fullPath;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$fullPath = fullPath;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$fullPath,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other
            is Fragment$UserProjects$projectMemberships$nodes$project$namespace) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$fullPath = fullPath;
    final lOther$fullPath = other.fullPath;
    if (l$fullPath != lOther$fullPath) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$UserProjects$projectMemberships$nodes$project$namespace
    on Fragment$UserProjects$projectMemberships$nodes$project$namespace {
  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$namespace<
          Fragment$UserProjects$projectMemberships$nodes$project$namespace>
      get copyWith =>
          CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$namespace(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$namespace<
    TRes> {
  factory CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$namespace(
    Fragment$UserProjects$projectMemberships$nodes$project$namespace instance,
    TRes Function(
            Fragment$UserProjects$projectMemberships$nodes$project$namespace)
        then,
  ) = _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$namespace;

  factory CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$namespace.stub(
          TRes res) =
      _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$namespace;

  TRes call({
    String? fullPath,
    String? $__typename,
  });
}

class _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$namespace<
        TRes>
    implements
        CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$namespace<
            TRes> {
  _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$namespace(
    this._instance,
    this._then,
  );

  final Fragment$UserProjects$projectMemberships$nodes$project$namespace
      _instance;

  final TRes Function(
      Fragment$UserProjects$projectMemberships$nodes$project$namespace) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? fullPath = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$UserProjects$projectMemberships$nodes$project$namespace(
        fullPath: fullPath == _undefined || fullPath == null
            ? _instance.fullPath
            : (fullPath as String),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$namespace<
        TRes>
    implements
        CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$namespace<
            TRes> {
  _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$namespace(
      this._res);

  TRes _res;

  call({
    String? fullPath,
    String? $__typename,
  }) =>
      _res;
}

class Fragment$UserProjects$projectMemberships$nodes$project$repository
    implements Fragment$ProjectFields$repository {
  Fragment$UserProjects$projectMemberships$nodes$project$repository({
    this.defaultBranch,
    this.$__typename = 'Repository',
  });

  factory Fragment$UserProjects$projectMemberships$nodes$project$repository.fromJson(
      Map<String, dynamic> json) {
    final l$defaultBranch = json['defaultBranch'];
    final l$$__typename = json['__typename'];
    return Fragment$UserProjects$projectMemberships$nodes$project$repository(
      defaultBranch: (l$defaultBranch as String?),
      $__typename: (l$$__typename as String),
    );
  }

  final String? defaultBranch;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$defaultBranch = defaultBranch;
    _resultData['defaultBranch'] = l$defaultBranch;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$defaultBranch = defaultBranch;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$defaultBranch,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other
            is Fragment$UserProjects$projectMemberships$nodes$project$repository) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$defaultBranch = defaultBranch;
    final lOther$defaultBranch = other.defaultBranch;
    if (l$defaultBranch != lOther$defaultBranch) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$UserProjects$projectMemberships$nodes$project$repository
    on Fragment$UserProjects$projectMemberships$nodes$project$repository {
  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$repository<
          Fragment$UserProjects$projectMemberships$nodes$project$repository>
      get copyWith =>
          CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$repository(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$repository<
    TRes> {
  factory CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$repository(
    Fragment$UserProjects$projectMemberships$nodes$project$repository instance,
    TRes Function(
            Fragment$UserProjects$projectMemberships$nodes$project$repository)
        then,
  ) = _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$repository;

  factory CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$repository.stub(
          TRes res) =
      _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$repository;

  TRes call({
    String? defaultBranch,
    String? $__typename,
  });
}

class _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$repository<
        TRes>
    implements
        CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$repository<
            TRes> {
  _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$repository(
    this._instance,
    this._then,
  );

  final Fragment$UserProjects$projectMemberships$nodes$project$repository
      _instance;

  final TRes Function(
      Fragment$UserProjects$projectMemberships$nodes$project$repository) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? defaultBranch = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$UserProjects$projectMemberships$nodes$project$repository(
        defaultBranch: defaultBranch == _undefined
            ? _instance.defaultBranch
            : (defaultBranch as String?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$repository<
        TRes>
    implements
        CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$repository<
            TRes> {
  _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$repository(
      this._res);

  TRes _res;

  call({
    String? defaultBranch,
    String? $__typename,
  }) =>
      _res;
}

class Fragment$UserProjects$projectMemberships$nodes$project$pipelines
    implements Fragment$ProjectFields$pipelines {
  Fragment$UserProjects$projectMemberships$nodes$project$pipelines({
    this.nodes,
    this.$__typename = 'PipelineConnection',
  });

  factory Fragment$UserProjects$projectMemberships$nodes$project$pipelines.fromJson(
      Map<String, dynamic> json) {
    final l$nodes = json['nodes'];
    final l$$__typename = json['__typename'];
    return Fragment$UserProjects$projectMemberships$nodes$project$pipelines(
      nodes: (l$nodes as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Fragment$PipelineFields.fromJson((e as Map<String, dynamic>)))
          .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final List<Fragment$PipelineFields?>? nodes;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$nodes = nodes;
    _resultData['nodes'] = l$nodes?.map((e) => e?.toJson()).toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$nodes = nodes;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$nodes == null ? null : Object.hashAll(l$nodes.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other
            is Fragment$UserProjects$projectMemberships$nodes$project$pipelines) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$nodes = nodes;
    final lOther$nodes = other.nodes;
    if (l$nodes != null && lOther$nodes != null) {
      if (l$nodes.length != lOther$nodes.length) {
        return false;
      }
      for (int i = 0; i < l$nodes.length; i++) {
        final l$nodes$entry = l$nodes[i];
        final lOther$nodes$entry = lOther$nodes[i];
        if (l$nodes$entry != lOther$nodes$entry) {
          return false;
        }
      }
    } else if (l$nodes != lOther$nodes) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$UserProjects$projectMemberships$nodes$project$pipelines
    on Fragment$UserProjects$projectMemberships$nodes$project$pipelines {
  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$pipelines<
          Fragment$UserProjects$projectMemberships$nodes$project$pipelines>
      get copyWith =>
          CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$pipelines(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$pipelines<
    TRes> {
  factory CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$pipelines(
    Fragment$UserProjects$projectMemberships$nodes$project$pipelines instance,
    TRes Function(
            Fragment$UserProjects$projectMemberships$nodes$project$pipelines)
        then,
  ) = _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$pipelines;

  factory CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$pipelines.stub(
          TRes res) =
      _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$pipelines;

  TRes call({
    List<Fragment$PipelineFields?>? nodes,
    String? $__typename,
  });
  TRes nodes(
      Iterable<Fragment$PipelineFields?>? Function(
              Iterable<
                  CopyWith$Fragment$PipelineFields<Fragment$PipelineFields>?>?)
          _fn);
}

class _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$pipelines<
        TRes>
    implements
        CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$pipelines<
            TRes> {
  _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$pipelines(
    this._instance,
    this._then,
  );

  final Fragment$UserProjects$projectMemberships$nodes$project$pipelines
      _instance;

  final TRes Function(
      Fragment$UserProjects$projectMemberships$nodes$project$pipelines) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? nodes = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$UserProjects$projectMemberships$nodes$project$pipelines(
        nodes: nodes == _undefined
            ? _instance.nodes
            : (nodes as List<Fragment$PipelineFields?>?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  TRes nodes(
          Iterable<Fragment$PipelineFields?>? Function(
                  Iterable<
                      CopyWith$Fragment$PipelineFields<
                          Fragment$PipelineFields>?>?)
              _fn) =>
      call(
          nodes: _fn(_instance.nodes?.map((e) => e == null
              ? null
              : CopyWith$Fragment$PipelineFields(
                  e,
                  (i) => i,
                )))?.toList());
}

class _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$pipelines<
        TRes>
    implements
        CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$pipelines<
            TRes> {
  _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$pipelines(
      this._res);

  TRes _res;

  call({
    List<Fragment$PipelineFields?>? nodes,
    String? $__typename,
  }) =>
      _res;

  nodes(_fn) => _res;
}

class Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines
    implements Fragment$ProjectFields$tagPipelines {
  Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines({
    this.nodes,
    this.$__typename = 'PipelineConnection',
  });

  factory Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines.fromJson(
      Map<String, dynamic> json) {
    final l$nodes = json['nodes'];
    final l$$__typename = json['__typename'];
    return Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines(
      nodes: (l$nodes as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Fragment$PipelineFields.fromJson((e as Map<String, dynamic>)))
          .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final List<Fragment$PipelineFields?>? nodes;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$nodes = nodes;
    _resultData['nodes'] = l$nodes?.map((e) => e?.toJson()).toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$nodes = nodes;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$nodes == null ? null : Object.hashAll(l$nodes.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other
            is Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$nodes = nodes;
    final lOther$nodes = other.nodes;
    if (l$nodes != null && lOther$nodes != null) {
      if (l$nodes.length != lOther$nodes.length) {
        return false;
      }
      for (int i = 0; i < l$nodes.length; i++) {
        final l$nodes$entry = l$nodes[i];
        final lOther$nodes$entry = lOther$nodes[i];
        if (l$nodes$entry != lOther$nodes$entry) {
          return false;
        }
      }
    } else if (l$nodes != lOther$nodes) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines
    on Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines {
  CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines<
          Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines>
      get copyWith =>
          CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines<
    TRes> {
  factory CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines(
    Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines
        instance,
    TRes Function(
            Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines)
        then,
  ) = _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines;

  factory CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines.stub(
          TRes res) =
      _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines;

  TRes call({
    List<Fragment$PipelineFields?>? nodes,
    String? $__typename,
  });
  TRes nodes(
      Iterable<Fragment$PipelineFields?>? Function(
              Iterable<
                  CopyWith$Fragment$PipelineFields<Fragment$PipelineFields>?>?)
          _fn);
}

class _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines<
        TRes>
    implements
        CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines<
            TRes> {
  _CopyWithImpl$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines(
    this._instance,
    this._then,
  );

  final Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines
      _instance;

  final TRes Function(
          Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines)
      _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? nodes = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines(
        nodes: nodes == _undefined
            ? _instance.nodes
            : (nodes as List<Fragment$PipelineFields?>?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  TRes nodes(
          Iterable<Fragment$PipelineFields?>? Function(
                  Iterable<
                      CopyWith$Fragment$PipelineFields<
                          Fragment$PipelineFields>?>?)
              _fn) =>
      call(
          nodes: _fn(_instance.nodes?.map((e) => e == null
              ? null
              : CopyWith$Fragment$PipelineFields(
                  e,
                  (i) => i,
                )))?.toList());
}

class _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines<
        TRes>
    implements
        CopyWith$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines<
            TRes> {
  _CopyWithStubImpl$Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines(
      this._res);

  TRes _res;

  call({
    List<Fragment$PipelineFields?>? nodes,
    String? $__typename,
  }) =>
      _res;

  nodes(_fn) => _res;
}

class Fragment$ProjectFields {
  Fragment$ProjectFields({
    required this.name,
    required this.fullPath,
    this.repository,
    this.pipelines,
    this.tagPipelines,
    this.$__typename = 'Project',
  });

  factory Fragment$ProjectFields.fromJson(Map<String, dynamic> json) {
    final l$name = json['name'];
    final l$fullPath = json['fullPath'];
    final l$repository = json['repository'];
    final l$pipelines = json['pipelines'];
    final l$tagPipelines = json['tagPipelines'];
    final l$$__typename = json['__typename'];
    return Fragment$ProjectFields(
      name: (l$name as String),
      fullPath: (l$fullPath as String),
      repository: l$repository == null
          ? null
          : Fragment$ProjectFields$repository.fromJson(
              (l$repository as Map<String, dynamic>)),
      pipelines: l$pipelines == null
          ? null
          : Fragment$ProjectFields$pipelines.fromJson(
              (l$pipelines as Map<String, dynamic>)),
      tagPipelines: l$tagPipelines == null
          ? null
          : Fragment$ProjectFields$tagPipelines.fromJson(
              (l$tagPipelines as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final String name;

  final String fullPath;

  final Fragment$ProjectFields$repository? repository;

  final Fragment$ProjectFields$pipelines? pipelines;

  final Fragment$ProjectFields$tagPipelines? tagPipelines;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$name = name;
    _resultData['name'] = l$name;
    final l$fullPath = fullPath;
    _resultData['fullPath'] = l$fullPath;
    final l$repository = repository;
    _resultData['repository'] = l$repository?.toJson();
    final l$pipelines = pipelines;
    _resultData['pipelines'] = l$pipelines?.toJson();
    final l$tagPipelines = tagPipelines;
    _resultData['tagPipelines'] = l$tagPipelines?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$name = name;
    final l$fullPath = fullPath;
    final l$repository = repository;
    final l$pipelines = pipelines;
    final l$tagPipelines = tagPipelines;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$name,
      l$fullPath,
      l$repository,
      l$pipelines,
      l$tagPipelines,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$ProjectFields) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$name = name;
    final lOther$name = other.name;
    if (l$name != lOther$name) {
      return false;
    }
    final l$fullPath = fullPath;
    final lOther$fullPath = other.fullPath;
    if (l$fullPath != lOther$fullPath) {
      return false;
    }
    final l$repository = repository;
    final lOther$repository = other.repository;
    if (l$repository != lOther$repository) {
      return false;
    }
    final l$pipelines = pipelines;
    final lOther$pipelines = other.pipelines;
    if (l$pipelines != lOther$pipelines) {
      return false;
    }
    final l$tagPipelines = tagPipelines;
    final lOther$tagPipelines = other.tagPipelines;
    if (l$tagPipelines != lOther$tagPipelines) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$ProjectFields on Fragment$ProjectFields {
  CopyWith$Fragment$ProjectFields<Fragment$ProjectFields> get copyWith =>
      CopyWith$Fragment$ProjectFields(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Fragment$ProjectFields<TRes> {
  factory CopyWith$Fragment$ProjectFields(
    Fragment$ProjectFields instance,
    TRes Function(Fragment$ProjectFields) then,
  ) = _CopyWithImpl$Fragment$ProjectFields;

  factory CopyWith$Fragment$ProjectFields.stub(TRes res) =
      _CopyWithStubImpl$Fragment$ProjectFields;

  TRes call({
    String? name,
    String? fullPath,
    Fragment$ProjectFields$repository? repository,
    Fragment$ProjectFields$pipelines? pipelines,
    Fragment$ProjectFields$tagPipelines? tagPipelines,
    String? $__typename,
  });
  CopyWith$Fragment$ProjectFields$repository<TRes> get repository;
  CopyWith$Fragment$ProjectFields$pipelines<TRes> get pipelines;
  CopyWith$Fragment$ProjectFields$tagPipelines<TRes> get tagPipelines;
}

class _CopyWithImpl$Fragment$ProjectFields<TRes>
    implements CopyWith$Fragment$ProjectFields<TRes> {
  _CopyWithImpl$Fragment$ProjectFields(
    this._instance,
    this._then,
  );

  final Fragment$ProjectFields _instance;

  final TRes Function(Fragment$ProjectFields) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? name = _undefined,
    Object? fullPath = _undefined,
    Object? repository = _undefined,
    Object? pipelines = _undefined,
    Object? tagPipelines = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$ProjectFields(
        name: name == _undefined || name == null
            ? _instance.name
            : (name as String),
        fullPath: fullPath == _undefined || fullPath == null
            ? _instance.fullPath
            : (fullPath as String),
        repository: repository == _undefined
            ? _instance.repository
            : (repository as Fragment$ProjectFields$repository?),
        pipelines: pipelines == _undefined
            ? _instance.pipelines
            : (pipelines as Fragment$ProjectFields$pipelines?),
        tagPipelines: tagPipelines == _undefined
            ? _instance.tagPipelines
            : (tagPipelines as Fragment$ProjectFields$tagPipelines?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Fragment$ProjectFields$repository<TRes> get repository {
    final local$repository = _instance.repository;
    return local$repository == null
        ? CopyWith$Fragment$ProjectFields$repository.stub(_then(_instance))
        : CopyWith$Fragment$ProjectFields$repository(
            local$repository, (e) => call(repository: e));
  }

  CopyWith$Fragment$ProjectFields$pipelines<TRes> get pipelines {
    final local$pipelines = _instance.pipelines;
    return local$pipelines == null
        ? CopyWith$Fragment$ProjectFields$pipelines.stub(_then(_instance))
        : CopyWith$Fragment$ProjectFields$pipelines(
            local$pipelines, (e) => call(pipelines: e));
  }

  CopyWith$Fragment$ProjectFields$tagPipelines<TRes> get tagPipelines {
    final local$tagPipelines = _instance.tagPipelines;
    return local$tagPipelines == null
        ? CopyWith$Fragment$ProjectFields$tagPipelines.stub(_then(_instance))
        : CopyWith$Fragment$ProjectFields$tagPipelines(
            local$tagPipelines, (e) => call(tagPipelines: e));
  }
}

class _CopyWithStubImpl$Fragment$ProjectFields<TRes>
    implements CopyWith$Fragment$ProjectFields<TRes> {
  _CopyWithStubImpl$Fragment$ProjectFields(this._res);

  TRes _res;

  call({
    String? name,
    String? fullPath,
    Fragment$ProjectFields$repository? repository,
    Fragment$ProjectFields$pipelines? pipelines,
    Fragment$ProjectFields$tagPipelines? tagPipelines,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Fragment$ProjectFields$repository<TRes> get repository =>
      CopyWith$Fragment$ProjectFields$repository.stub(_res);

  CopyWith$Fragment$ProjectFields$pipelines<TRes> get pipelines =>
      CopyWith$Fragment$ProjectFields$pipelines.stub(_res);

  CopyWith$Fragment$ProjectFields$tagPipelines<TRes> get tagPipelines =>
      CopyWith$Fragment$ProjectFields$tagPipelines.stub(_res);
}

const fragmentDefinitionProjectFields = FragmentDefinitionNode(
  name: NameNode(value: 'ProjectFields'),
  typeCondition: TypeConditionNode(
      on: NamedTypeNode(
    name: NameNode(value: 'Project'),
    isNonNull: false,
  )),
  directives: [],
  selectionSet: SelectionSetNode(selections: [
    FieldNode(
      name: NameNode(value: 'name'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'fullPath'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'repository'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
          name: NameNode(value: 'rootRef'),
          alias: NameNode(value: 'defaultBranch'),
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
        FieldNode(
          name: NameNode(value: '__typename'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
      ]),
    ),
    FieldNode(
      name: NameNode(value: 'pipelines'),
      alias: null,
      arguments: [
        ArgumentNode(
          name: NameNode(value: 'first'),
          value: IntValueNode(value: '10'),
        ),
        ArgumentNode(
          name: NameNode(value: 'scope'),
          value: EnumValueNode(name: NameNode(value: 'BRANCHES')),
        ),
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
          name: NameNode(value: 'nodes'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: SelectionSetNode(selections: [
            FragmentSpreadNode(
              name: NameNode(value: 'PipelineFields'),
              directives: [],
            ),
            FieldNode(
              name: NameNode(value: '__typename'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: null,
            ),
          ]),
        ),
        FieldNode(
          name: NameNode(value: '__typename'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
      ]),
    ),
    FieldNode(
      name: NameNode(value: 'pipelines'),
      alias: NameNode(value: 'tagPipelines'),
      arguments: [
        ArgumentNode(
          name: NameNode(value: 'first'),
          value: IntValueNode(value: '1'),
        ),
        ArgumentNode(
          name: NameNode(value: 'scope'),
          value: EnumValueNode(name: NameNode(value: 'TAGS')),
        ),
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
          name: NameNode(value: 'nodes'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: SelectionSetNode(selections: [
            FragmentSpreadNode(
              name: NameNode(value: 'PipelineFields'),
              directives: [],
            ),
            FieldNode(
              name: NameNode(value: '__typename'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: null,
            ),
          ]),
        ),
        FieldNode(
          name: NameNode(value: '__typename'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
      ]),
    ),
    FieldNode(
      name: NameNode(value: '__typename'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
  ]),
);
const documentNodeFragmentProjectFields = DocumentNode(definitions: [
  fragmentDefinitionProjectFields,
  fragmentDefinitionPipelineFields,
]);

class Fragment$ProjectFields$repository {
  Fragment$ProjectFields$repository({
    this.defaultBranch,
    this.$__typename = 'Repository',
  });

  factory Fragment$ProjectFields$repository.fromJson(
      Map<String, dynamic> json) {
    final l$defaultBranch = json['defaultBranch'];
    final l$$__typename = json['__typename'];
    return Fragment$ProjectFields$repository(
      defaultBranch: (l$defaultBranch as String?),
      $__typename: (l$$__typename as String),
    );
  }

  final String? defaultBranch;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$defaultBranch = defaultBranch;
    _resultData['defaultBranch'] = l$defaultBranch;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$defaultBranch = defaultBranch;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$defaultBranch,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$ProjectFields$repository) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$defaultBranch = defaultBranch;
    final lOther$defaultBranch = other.defaultBranch;
    if (l$defaultBranch != lOther$defaultBranch) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$ProjectFields$repository
    on Fragment$ProjectFields$repository {
  CopyWith$Fragment$ProjectFields$repository<Fragment$ProjectFields$repository>
      get copyWith => CopyWith$Fragment$ProjectFields$repository(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$ProjectFields$repository<TRes> {
  factory CopyWith$Fragment$ProjectFields$repository(
    Fragment$ProjectFields$repository instance,
    TRes Function(Fragment$ProjectFields$repository) then,
  ) = _CopyWithImpl$Fragment$ProjectFields$repository;

  factory CopyWith$Fragment$ProjectFields$repository.stub(TRes res) =
      _CopyWithStubImpl$Fragment$ProjectFields$repository;

  TRes call({
    String? defaultBranch,
    String? $__typename,
  });
}

class _CopyWithImpl$Fragment$ProjectFields$repository<TRes>
    implements CopyWith$Fragment$ProjectFields$repository<TRes> {
  _CopyWithImpl$Fragment$ProjectFields$repository(
    this._instance,
    this._then,
  );

  final Fragment$ProjectFields$repository _instance;

  final TRes Function(Fragment$ProjectFields$repository) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? defaultBranch = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$ProjectFields$repository(
        defaultBranch: defaultBranch == _undefined
            ? _instance.defaultBranch
            : (defaultBranch as String?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Fragment$ProjectFields$repository<TRes>
    implements CopyWith$Fragment$ProjectFields$repository<TRes> {
  _CopyWithStubImpl$Fragment$ProjectFields$repository(this._res);

  TRes _res;

  call({
    String? defaultBranch,
    String? $__typename,
  }) =>
      _res;
}

class Fragment$ProjectFields$pipelines {
  Fragment$ProjectFields$pipelines({
    this.nodes,
    this.$__typename = 'PipelineConnection',
  });

  factory Fragment$ProjectFields$pipelines.fromJson(Map<String, dynamic> json) {
    final l$nodes = json['nodes'];
    final l$$__typename = json['__typename'];
    return Fragment$ProjectFields$pipelines(
      nodes: (l$nodes as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Fragment$PipelineFields.fromJson((e as Map<String, dynamic>)))
          .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final List<Fragment$PipelineFields?>? nodes;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$nodes = nodes;
    _resultData['nodes'] = l$nodes?.map((e) => e?.toJson()).toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$nodes = nodes;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$nodes == null ? null : Object.hashAll(l$nodes.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$ProjectFields$pipelines) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$nodes = nodes;
    final lOther$nodes = other.nodes;
    if (l$nodes != null && lOther$nodes != null) {
      if (l$nodes.length != lOther$nodes.length) {
        return false;
      }
      for (int i = 0; i < l$nodes.length; i++) {
        final l$nodes$entry = l$nodes[i];
        final lOther$nodes$entry = lOther$nodes[i];
        if (l$nodes$entry != lOther$nodes$entry) {
          return false;
        }
      }
    } else if (l$nodes != lOther$nodes) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$ProjectFields$pipelines
    on Fragment$ProjectFields$pipelines {
  CopyWith$Fragment$ProjectFields$pipelines<Fragment$ProjectFields$pipelines>
      get copyWith => CopyWith$Fragment$ProjectFields$pipelines(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$ProjectFields$pipelines<TRes> {
  factory CopyWith$Fragment$ProjectFields$pipelines(
    Fragment$ProjectFields$pipelines instance,
    TRes Function(Fragment$ProjectFields$pipelines) then,
  ) = _CopyWithImpl$Fragment$ProjectFields$pipelines;

  factory CopyWith$Fragment$ProjectFields$pipelines.stub(TRes res) =
      _CopyWithStubImpl$Fragment$ProjectFields$pipelines;

  TRes call({
    List<Fragment$PipelineFields?>? nodes,
    String? $__typename,
  });
  TRes nodes(
      Iterable<Fragment$PipelineFields?>? Function(
              Iterable<
                  CopyWith$Fragment$PipelineFields<Fragment$PipelineFields>?>?)
          _fn);
}

class _CopyWithImpl$Fragment$ProjectFields$pipelines<TRes>
    implements CopyWith$Fragment$ProjectFields$pipelines<TRes> {
  _CopyWithImpl$Fragment$ProjectFields$pipelines(
    this._instance,
    this._then,
  );

  final Fragment$ProjectFields$pipelines _instance;

  final TRes Function(Fragment$ProjectFields$pipelines) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? nodes = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$ProjectFields$pipelines(
        nodes: nodes == _undefined
            ? _instance.nodes
            : (nodes as List<Fragment$PipelineFields?>?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  TRes nodes(
          Iterable<Fragment$PipelineFields?>? Function(
                  Iterable<
                      CopyWith$Fragment$PipelineFields<
                          Fragment$PipelineFields>?>?)
              _fn) =>
      call(
          nodes: _fn(_instance.nodes?.map((e) => e == null
              ? null
              : CopyWith$Fragment$PipelineFields(
                  e,
                  (i) => i,
                )))?.toList());
}

class _CopyWithStubImpl$Fragment$ProjectFields$pipelines<TRes>
    implements CopyWith$Fragment$ProjectFields$pipelines<TRes> {
  _CopyWithStubImpl$Fragment$ProjectFields$pipelines(this._res);

  TRes _res;

  call({
    List<Fragment$PipelineFields?>? nodes,
    String? $__typename,
  }) =>
      _res;

  nodes(_fn) => _res;
}

class Fragment$ProjectFields$tagPipelines {
  Fragment$ProjectFields$tagPipelines({
    this.nodes,
    this.$__typename = 'PipelineConnection',
  });

  factory Fragment$ProjectFields$tagPipelines.fromJson(
      Map<String, dynamic> json) {
    final l$nodes = json['nodes'];
    final l$$__typename = json['__typename'];
    return Fragment$ProjectFields$tagPipelines(
      nodes: (l$nodes as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Fragment$PipelineFields.fromJson((e as Map<String, dynamic>)))
          .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final List<Fragment$PipelineFields?>? nodes;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$nodes = nodes;
    _resultData['nodes'] = l$nodes?.map((e) => e?.toJson()).toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$nodes = nodes;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$nodes == null ? null : Object.hashAll(l$nodes.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$ProjectFields$tagPipelines) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$nodes = nodes;
    final lOther$nodes = other.nodes;
    if (l$nodes != null && lOther$nodes != null) {
      if (l$nodes.length != lOther$nodes.length) {
        return false;
      }
      for (int i = 0; i < l$nodes.length; i++) {
        final l$nodes$entry = l$nodes[i];
        final lOther$nodes$entry = lOther$nodes[i];
        if (l$nodes$entry != lOther$nodes$entry) {
          return false;
        }
      }
    } else if (l$nodes != lOther$nodes) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$ProjectFields$tagPipelines
    on Fragment$ProjectFields$tagPipelines {
  CopyWith$Fragment$ProjectFields$tagPipelines<
          Fragment$ProjectFields$tagPipelines>
      get copyWith => CopyWith$Fragment$ProjectFields$tagPipelines(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$ProjectFields$tagPipelines<TRes> {
  factory CopyWith$Fragment$ProjectFields$tagPipelines(
    Fragment$ProjectFields$tagPipelines instance,
    TRes Function(Fragment$ProjectFields$tagPipelines) then,
  ) = _CopyWithImpl$Fragment$ProjectFields$tagPipelines;

  factory CopyWith$Fragment$ProjectFields$tagPipelines.stub(TRes res) =
      _CopyWithStubImpl$Fragment$ProjectFields$tagPipelines;

  TRes call({
    List<Fragment$PipelineFields?>? nodes,
    String? $__typename,
  });
  TRes nodes(
      Iterable<Fragment$PipelineFields?>? Function(
              Iterable<
                  CopyWith$Fragment$PipelineFields<Fragment$PipelineFields>?>?)
          _fn);
}

class _CopyWithImpl$Fragment$ProjectFields$tagPipelines<TRes>
    implements CopyWith$Fragment$ProjectFields$tagPipelines<TRes> {
  _CopyWithImpl$Fragment$ProjectFields$tagPipelines(
    this._instance,
    this._then,
  );

  final Fragment$ProjectFields$tagPipelines _instance;

  final TRes Function(Fragment$ProjectFields$tagPipelines) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? nodes = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$ProjectFields$tagPipelines(
        nodes: nodes == _undefined
            ? _instance.nodes
            : (nodes as List<Fragment$PipelineFields?>?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  TRes nodes(
          Iterable<Fragment$PipelineFields?>? Function(
                  Iterable<
                      CopyWith$Fragment$PipelineFields<
                          Fragment$PipelineFields>?>?)
              _fn) =>
      call(
          nodes: _fn(_instance.nodes?.map((e) => e == null
              ? null
              : CopyWith$Fragment$PipelineFields(
                  e,
                  (i) => i,
                )))?.toList());
}

class _CopyWithStubImpl$Fragment$ProjectFields$tagPipelines<TRes>
    implements CopyWith$Fragment$ProjectFields$tagPipelines<TRes> {
  _CopyWithStubImpl$Fragment$ProjectFields$tagPipelines(this._res);

  TRes _res;

  call({
    List<Fragment$PipelineFields?>? nodes,
    String? $__typename,
  }) =>
      _res;

  nodes(_fn) => _res;
}

class Fragment$PipelineFields {
  Fragment$PipelineFields({
    this.sha,
    this.ref,
    this.duration,
    required this.pipelineDate,
    this.commit,
    required this.detailedStatus,
    this.stages,
    this.$__typename = 'Pipeline',
  });

  factory Fragment$PipelineFields.fromJson(Map<String, dynamic> json) {
    final l$sha = json['sha'];
    final l$ref = json['ref'];
    final l$duration = json['duration'];
    final l$pipelineDate = json['pipelineDate'];
    final l$commit = json['commit'];
    final l$detailedStatus = json['detailedStatus'];
    final l$stages = json['stages'];
    final l$$__typename = json['__typename'];
    return Fragment$PipelineFields(
      sha: (l$sha as String?),
      ref: (l$ref as String?),
      duration: (l$duration as int?),
      pipelineDate: (l$pipelineDate as String),
      commit: l$commit == null
          ? null
          : Fragment$PipelineFields$commit.fromJson(
              (l$commit as Map<String, dynamic>)),
      detailedStatus: Fragment$PipelineFields$detailedStatus.fromJson(
          (l$detailedStatus as Map<String, dynamic>)),
      stages: l$stages == null
          ? null
          : Fragment$PipelineFields$stages.fromJson(
              (l$stages as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final String? sha;

  final String? ref;

  final int? duration;

  final String pipelineDate;

  final Fragment$PipelineFields$commit? commit;

  final Fragment$PipelineFields$detailedStatus detailedStatus;

  final Fragment$PipelineFields$stages? stages;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$sha = sha;
    _resultData['sha'] = l$sha;
    final l$ref = ref;
    _resultData['ref'] = l$ref;
    final l$duration = duration;
    _resultData['duration'] = l$duration;
    final l$pipelineDate = pipelineDate;
    _resultData['pipelineDate'] = l$pipelineDate;
    final l$commit = commit;
    _resultData['commit'] = l$commit?.toJson();
    final l$detailedStatus = detailedStatus;
    _resultData['detailedStatus'] = l$detailedStatus.toJson();
    final l$stages = stages;
    _resultData['stages'] = l$stages?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$sha = sha;
    final l$ref = ref;
    final l$duration = duration;
    final l$pipelineDate = pipelineDate;
    final l$commit = commit;
    final l$detailedStatus = detailedStatus;
    final l$stages = stages;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$sha,
      l$ref,
      l$duration,
      l$pipelineDate,
      l$commit,
      l$detailedStatus,
      l$stages,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$PipelineFields) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$sha = sha;
    final lOther$sha = other.sha;
    if (l$sha != lOther$sha) {
      return false;
    }
    final l$ref = ref;
    final lOther$ref = other.ref;
    if (l$ref != lOther$ref) {
      return false;
    }
    final l$duration = duration;
    final lOther$duration = other.duration;
    if (l$duration != lOther$duration) {
      return false;
    }
    final l$pipelineDate = pipelineDate;
    final lOther$pipelineDate = other.pipelineDate;
    if (l$pipelineDate != lOther$pipelineDate) {
      return false;
    }
    final l$commit = commit;
    final lOther$commit = other.commit;
    if (l$commit != lOther$commit) {
      return false;
    }
    final l$detailedStatus = detailedStatus;
    final lOther$detailedStatus = other.detailedStatus;
    if (l$detailedStatus != lOther$detailedStatus) {
      return false;
    }
    final l$stages = stages;
    final lOther$stages = other.stages;
    if (l$stages != lOther$stages) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$PipelineFields on Fragment$PipelineFields {
  CopyWith$Fragment$PipelineFields<Fragment$PipelineFields> get copyWith =>
      CopyWith$Fragment$PipelineFields(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Fragment$PipelineFields<TRes> {
  factory CopyWith$Fragment$PipelineFields(
    Fragment$PipelineFields instance,
    TRes Function(Fragment$PipelineFields) then,
  ) = _CopyWithImpl$Fragment$PipelineFields;

  factory CopyWith$Fragment$PipelineFields.stub(TRes res) =
      _CopyWithStubImpl$Fragment$PipelineFields;

  TRes call({
    String? sha,
    String? ref,
    int? duration,
    String? pipelineDate,
    Fragment$PipelineFields$commit? commit,
    Fragment$PipelineFields$detailedStatus? detailedStatus,
    Fragment$PipelineFields$stages? stages,
    String? $__typename,
  });
  CopyWith$Fragment$PipelineFields$commit<TRes> get commit;
  CopyWith$Fragment$PipelineFields$detailedStatus<TRes> get detailedStatus;
  CopyWith$Fragment$PipelineFields$stages<TRes> get stages;
}

class _CopyWithImpl$Fragment$PipelineFields<TRes>
    implements CopyWith$Fragment$PipelineFields<TRes> {
  _CopyWithImpl$Fragment$PipelineFields(
    this._instance,
    this._then,
  );

  final Fragment$PipelineFields _instance;

  final TRes Function(Fragment$PipelineFields) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? sha = _undefined,
    Object? ref = _undefined,
    Object? duration = _undefined,
    Object? pipelineDate = _undefined,
    Object? commit = _undefined,
    Object? detailedStatus = _undefined,
    Object? stages = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$PipelineFields(
        sha: sha == _undefined ? _instance.sha : (sha as String?),
        ref: ref == _undefined ? _instance.ref : (ref as String?),
        duration:
            duration == _undefined ? _instance.duration : (duration as int?),
        pipelineDate: pipelineDate == _undefined || pipelineDate == null
            ? _instance.pipelineDate
            : (pipelineDate as String),
        commit: commit == _undefined
            ? _instance.commit
            : (commit as Fragment$PipelineFields$commit?),
        detailedStatus: detailedStatus == _undefined || detailedStatus == null
            ? _instance.detailedStatus
            : (detailedStatus as Fragment$PipelineFields$detailedStatus),
        stages: stages == _undefined
            ? _instance.stages
            : (stages as Fragment$PipelineFields$stages?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Fragment$PipelineFields$commit<TRes> get commit {
    final local$commit = _instance.commit;
    return local$commit == null
        ? CopyWith$Fragment$PipelineFields$commit.stub(_then(_instance))
        : CopyWith$Fragment$PipelineFields$commit(
            local$commit, (e) => call(commit: e));
  }

  CopyWith$Fragment$PipelineFields$detailedStatus<TRes> get detailedStatus {
    final local$detailedStatus = _instance.detailedStatus;
    return CopyWith$Fragment$PipelineFields$detailedStatus(
        local$detailedStatus, (e) => call(detailedStatus: e));
  }

  CopyWith$Fragment$PipelineFields$stages<TRes> get stages {
    final local$stages = _instance.stages;
    return local$stages == null
        ? CopyWith$Fragment$PipelineFields$stages.stub(_then(_instance))
        : CopyWith$Fragment$PipelineFields$stages(
            local$stages, (e) => call(stages: e));
  }
}

class _CopyWithStubImpl$Fragment$PipelineFields<TRes>
    implements CopyWith$Fragment$PipelineFields<TRes> {
  _CopyWithStubImpl$Fragment$PipelineFields(this._res);

  TRes _res;

  call({
    String? sha,
    String? ref,
    int? duration,
    String? pipelineDate,
    Fragment$PipelineFields$commit? commit,
    Fragment$PipelineFields$detailedStatus? detailedStatus,
    Fragment$PipelineFields$stages? stages,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Fragment$PipelineFields$commit<TRes> get commit =>
      CopyWith$Fragment$PipelineFields$commit.stub(_res);

  CopyWith$Fragment$PipelineFields$detailedStatus<TRes> get detailedStatus =>
      CopyWith$Fragment$PipelineFields$detailedStatus.stub(_res);

  CopyWith$Fragment$PipelineFields$stages<TRes> get stages =>
      CopyWith$Fragment$PipelineFields$stages.stub(_res);
}

const fragmentDefinitionPipelineFields = FragmentDefinitionNode(
  name: NameNode(value: 'PipelineFields'),
  typeCondition: TypeConditionNode(
      on: NamedTypeNode(
    name: NameNode(value: 'Pipeline'),
    isNonNull: false,
  )),
  directives: [],
  selectionSet: SelectionSetNode(selections: [
    FieldNode(
      name: NameNode(value: 'sha'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'ref'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'duration'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'updatedAt'),
      alias: NameNode(value: 'pipelineDate'),
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'commit'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
          name: NameNode(value: 'title'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
        FieldNode(
          name: NameNode(value: '__typename'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
      ]),
    ),
    FieldNode(
      name: NameNode(value: 'detailedStatus'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
          name: NameNode(value: 'icon'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
        FieldNode(
          name: NameNode(value: 'text'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
        FieldNode(
          name: NameNode(value: '__typename'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
      ]),
    ),
    FieldNode(
      name: NameNode(value: 'stages'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
          name: NameNode(value: 'nodes'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: SelectionSetNode(selections: [
            FieldNode(
              name: NameNode(value: 'name'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: null,
            ),
            FieldNode(
              name: NameNode(value: 'status'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: null,
            ),
            FieldNode(
              name: NameNode(value: 'detailedStatus'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                  name: NameNode(value: 'icon'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null,
                ),
                FieldNode(
                  name: NameNode(value: '__typename'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null,
                ),
              ]),
            ),
            FieldNode(
              name: NameNode(value: 'jobs'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                  name: NameNode(value: 'nodes'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                      name: NameNode(value: 'status'),
                      alias: null,
                      arguments: [],
                      directives: [],
                      selectionSet: null,
                    ),
                    FieldNode(
                      name: NameNode(value: '__typename'),
                      alias: null,
                      arguments: [],
                      directives: [],
                      selectionSet: null,
                    ),
                  ]),
                ),
                FieldNode(
                  name: NameNode(value: '__typename'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null,
                ),
              ]),
            ),
            FieldNode(
              name: NameNode(value: '__typename'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: null,
            ),
          ]),
        ),
        FieldNode(
          name: NameNode(value: '__typename'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
      ]),
    ),
    FieldNode(
      name: NameNode(value: '__typename'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
  ]),
);
const documentNodeFragmentPipelineFields = DocumentNode(definitions: [
  fragmentDefinitionPipelineFields,
]);

class Fragment$PipelineFields$commit {
  Fragment$PipelineFields$commit({
    this.title,
    this.$__typename = 'Commit',
  });

  factory Fragment$PipelineFields$commit.fromJson(Map<String, dynamic> json) {
    final l$title = json['title'];
    final l$$__typename = json['__typename'];
    return Fragment$PipelineFields$commit(
      title: (l$title as String?),
      $__typename: (l$$__typename as String),
    );
  }

  final String? title;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$title = title;
    _resultData['title'] = l$title;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$title = title;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$title,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$PipelineFields$commit) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$title = title;
    final lOther$title = other.title;
    if (l$title != lOther$title) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$PipelineFields$commit
    on Fragment$PipelineFields$commit {
  CopyWith$Fragment$PipelineFields$commit<Fragment$PipelineFields$commit>
      get copyWith => CopyWith$Fragment$PipelineFields$commit(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$PipelineFields$commit<TRes> {
  factory CopyWith$Fragment$PipelineFields$commit(
    Fragment$PipelineFields$commit instance,
    TRes Function(Fragment$PipelineFields$commit) then,
  ) = _CopyWithImpl$Fragment$PipelineFields$commit;

  factory CopyWith$Fragment$PipelineFields$commit.stub(TRes res) =
      _CopyWithStubImpl$Fragment$PipelineFields$commit;

  TRes call({
    String? title,
    String? $__typename,
  });
}

class _CopyWithImpl$Fragment$PipelineFields$commit<TRes>
    implements CopyWith$Fragment$PipelineFields$commit<TRes> {
  _CopyWithImpl$Fragment$PipelineFields$commit(
    this._instance,
    this._then,
  );

  final Fragment$PipelineFields$commit _instance;

  final TRes Function(Fragment$PipelineFields$commit) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? title = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$PipelineFields$commit(
        title: title == _undefined ? _instance.title : (title as String?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Fragment$PipelineFields$commit<TRes>
    implements CopyWith$Fragment$PipelineFields$commit<TRes> {
  _CopyWithStubImpl$Fragment$PipelineFields$commit(this._res);

  TRes _res;

  call({
    String? title,
    String? $__typename,
  }) =>
      _res;
}

class Fragment$PipelineFields$detailedStatus {
  Fragment$PipelineFields$detailedStatus({
    this.icon,
    this.text,
    this.$__typename = 'DetailedStatus',
  });

  factory Fragment$PipelineFields$detailedStatus.fromJson(
      Map<String, dynamic> json) {
    final l$icon = json['icon'];
    final l$text = json['text'];
    final l$$__typename = json['__typename'];
    return Fragment$PipelineFields$detailedStatus(
      icon: (l$icon as String?),
      text: (l$text as String?),
      $__typename: (l$$__typename as String),
    );
  }

  @Deprecated(
      'The `icon` attribute is deprecated. Use `name` to identify the status to display instead. Deprecated in 16.4.')
  final String? icon;

  @Deprecated(
      'The `text` attribute is being deprecated. Use `label` instead. Deprecated in 16.4.')
  final String? text;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$icon = icon;
    _resultData['icon'] = l$icon;
    final l$text = text;
    _resultData['text'] = l$text;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$icon = icon;
    final l$text = text;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$icon,
      l$text,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$PipelineFields$detailedStatus) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$icon = icon;
    final lOther$icon = other.icon;
    if (l$icon != lOther$icon) {
      return false;
    }
    final l$text = text;
    final lOther$text = other.text;
    if (l$text != lOther$text) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$PipelineFields$detailedStatus
    on Fragment$PipelineFields$detailedStatus {
  CopyWith$Fragment$PipelineFields$detailedStatus<
          Fragment$PipelineFields$detailedStatus>
      get copyWith => CopyWith$Fragment$PipelineFields$detailedStatus(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$PipelineFields$detailedStatus<TRes> {
  factory CopyWith$Fragment$PipelineFields$detailedStatus(
    Fragment$PipelineFields$detailedStatus instance,
    TRes Function(Fragment$PipelineFields$detailedStatus) then,
  ) = _CopyWithImpl$Fragment$PipelineFields$detailedStatus;

  factory CopyWith$Fragment$PipelineFields$detailedStatus.stub(TRes res) =
      _CopyWithStubImpl$Fragment$PipelineFields$detailedStatus;

  TRes call({
    String? icon,
    String? text,
    String? $__typename,
  });
}

class _CopyWithImpl$Fragment$PipelineFields$detailedStatus<TRes>
    implements CopyWith$Fragment$PipelineFields$detailedStatus<TRes> {
  _CopyWithImpl$Fragment$PipelineFields$detailedStatus(
    this._instance,
    this._then,
  );

  final Fragment$PipelineFields$detailedStatus _instance;

  final TRes Function(Fragment$PipelineFields$detailedStatus) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? icon = _undefined,
    Object? text = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$PipelineFields$detailedStatus(
        icon: icon == _undefined ? _instance.icon : (icon as String?),
        text: text == _undefined ? _instance.text : (text as String?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Fragment$PipelineFields$detailedStatus<TRes>
    implements CopyWith$Fragment$PipelineFields$detailedStatus<TRes> {
  _CopyWithStubImpl$Fragment$PipelineFields$detailedStatus(this._res);

  TRes _res;

  call({
    String? icon,
    String? text,
    String? $__typename,
  }) =>
      _res;
}

class Fragment$PipelineFields$stages {
  Fragment$PipelineFields$stages({
    this.nodes,
    this.$__typename = 'CiStageConnection',
  });

  factory Fragment$PipelineFields$stages.fromJson(Map<String, dynamic> json) {
    final l$nodes = json['nodes'];
    final l$$__typename = json['__typename'];
    return Fragment$PipelineFields$stages(
      nodes: (l$nodes as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Fragment$PipelineFields$stages$nodes.fromJson(
                  (e as Map<String, dynamic>)))
          .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final List<Fragment$PipelineFields$stages$nodes?>? nodes;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$nodes = nodes;
    _resultData['nodes'] = l$nodes?.map((e) => e?.toJson()).toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$nodes = nodes;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$nodes == null ? null : Object.hashAll(l$nodes.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$PipelineFields$stages) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$nodes = nodes;
    final lOther$nodes = other.nodes;
    if (l$nodes != null && lOther$nodes != null) {
      if (l$nodes.length != lOther$nodes.length) {
        return false;
      }
      for (int i = 0; i < l$nodes.length; i++) {
        final l$nodes$entry = l$nodes[i];
        final lOther$nodes$entry = lOther$nodes[i];
        if (l$nodes$entry != lOther$nodes$entry) {
          return false;
        }
      }
    } else if (l$nodes != lOther$nodes) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$PipelineFields$stages
    on Fragment$PipelineFields$stages {
  CopyWith$Fragment$PipelineFields$stages<Fragment$PipelineFields$stages>
      get copyWith => CopyWith$Fragment$PipelineFields$stages(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$PipelineFields$stages<TRes> {
  factory CopyWith$Fragment$PipelineFields$stages(
    Fragment$PipelineFields$stages instance,
    TRes Function(Fragment$PipelineFields$stages) then,
  ) = _CopyWithImpl$Fragment$PipelineFields$stages;

  factory CopyWith$Fragment$PipelineFields$stages.stub(TRes res) =
      _CopyWithStubImpl$Fragment$PipelineFields$stages;

  TRes call({
    List<Fragment$PipelineFields$stages$nodes?>? nodes,
    String? $__typename,
  });
  TRes nodes(
      Iterable<Fragment$PipelineFields$stages$nodes?>? Function(
              Iterable<
                  CopyWith$Fragment$PipelineFields$stages$nodes<
                      Fragment$PipelineFields$stages$nodes>?>?)
          _fn);
}

class _CopyWithImpl$Fragment$PipelineFields$stages<TRes>
    implements CopyWith$Fragment$PipelineFields$stages<TRes> {
  _CopyWithImpl$Fragment$PipelineFields$stages(
    this._instance,
    this._then,
  );

  final Fragment$PipelineFields$stages _instance;

  final TRes Function(Fragment$PipelineFields$stages) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? nodes = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$PipelineFields$stages(
        nodes: nodes == _undefined
            ? _instance.nodes
            : (nodes as List<Fragment$PipelineFields$stages$nodes?>?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  TRes nodes(
          Iterable<Fragment$PipelineFields$stages$nodes?>? Function(
                  Iterable<
                      CopyWith$Fragment$PipelineFields$stages$nodes<
                          Fragment$PipelineFields$stages$nodes>?>?)
              _fn) =>
      call(
          nodes: _fn(_instance.nodes?.map((e) => e == null
              ? null
              : CopyWith$Fragment$PipelineFields$stages$nodes(
                  e,
                  (i) => i,
                )))?.toList());
}

class _CopyWithStubImpl$Fragment$PipelineFields$stages<TRes>
    implements CopyWith$Fragment$PipelineFields$stages<TRes> {
  _CopyWithStubImpl$Fragment$PipelineFields$stages(this._res);

  TRes _res;

  call({
    List<Fragment$PipelineFields$stages$nodes?>? nodes,
    String? $__typename,
  }) =>
      _res;

  nodes(_fn) => _res;
}

class Fragment$PipelineFields$stages$nodes {
  Fragment$PipelineFields$stages$nodes({
    this.name,
    this.status,
    this.detailedStatus,
    this.jobs,
    this.$__typename = 'CiStage',
  });

  factory Fragment$PipelineFields$stages$nodes.fromJson(
      Map<String, dynamic> json) {
    final l$name = json['name'];
    final l$status = json['status'];
    final l$detailedStatus = json['detailedStatus'];
    final l$jobs = json['jobs'];
    final l$$__typename = json['__typename'];
    return Fragment$PipelineFields$stages$nodes(
      name: (l$name as String?),
      status: (l$status as String?),
      detailedStatus: l$detailedStatus == null
          ? null
          : Fragment$PipelineFields$stages$nodes$detailedStatus.fromJson(
              (l$detailedStatus as Map<String, dynamic>)),
      jobs: l$jobs == null
          ? null
          : Fragment$PipelineFields$stages$nodes$jobs.fromJson(
              (l$jobs as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final String? name;

  final String? status;

  final Fragment$PipelineFields$stages$nodes$detailedStatus? detailedStatus;

  final Fragment$PipelineFields$stages$nodes$jobs? jobs;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$name = name;
    _resultData['name'] = l$name;
    final l$status = status;
    _resultData['status'] = l$status;
    final l$detailedStatus = detailedStatus;
    _resultData['detailedStatus'] = l$detailedStatus?.toJson();
    final l$jobs = jobs;
    _resultData['jobs'] = l$jobs?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$name = name;
    final l$status = status;
    final l$detailedStatus = detailedStatus;
    final l$jobs = jobs;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$name,
      l$status,
      l$detailedStatus,
      l$jobs,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$PipelineFields$stages$nodes) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$name = name;
    final lOther$name = other.name;
    if (l$name != lOther$name) {
      return false;
    }
    final l$status = status;
    final lOther$status = other.status;
    if (l$status != lOther$status) {
      return false;
    }
    final l$detailedStatus = detailedStatus;
    final lOther$detailedStatus = other.detailedStatus;
    if (l$detailedStatus != lOther$detailedStatus) {
      return false;
    }
    final l$jobs = jobs;
    final lOther$jobs = other.jobs;
    if (l$jobs != lOther$jobs) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$PipelineFields$stages$nodes
    on Fragment$PipelineFields$stages$nodes {
  CopyWith$Fragment$PipelineFields$stages$nodes<
          Fragment$PipelineFields$stages$nodes>
      get copyWith => CopyWith$Fragment$PipelineFields$stages$nodes(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$PipelineFields$stages$nodes<TRes> {
  factory CopyWith$Fragment$PipelineFields$stages$nodes(
    Fragment$PipelineFields$stages$nodes instance,
    TRes Function(Fragment$PipelineFields$stages$nodes) then,
  ) = _CopyWithImpl$Fragment$PipelineFields$stages$nodes;

  factory CopyWith$Fragment$PipelineFields$stages$nodes.stub(TRes res) =
      _CopyWithStubImpl$Fragment$PipelineFields$stages$nodes;

  TRes call({
    String? name,
    String? status,
    Fragment$PipelineFields$stages$nodes$detailedStatus? detailedStatus,
    Fragment$PipelineFields$stages$nodes$jobs? jobs,
    String? $__typename,
  });
  CopyWith$Fragment$PipelineFields$stages$nodes$detailedStatus<TRes>
      get detailedStatus;
  CopyWith$Fragment$PipelineFields$stages$nodes$jobs<TRes> get jobs;
}

class _CopyWithImpl$Fragment$PipelineFields$stages$nodes<TRes>
    implements CopyWith$Fragment$PipelineFields$stages$nodes<TRes> {
  _CopyWithImpl$Fragment$PipelineFields$stages$nodes(
    this._instance,
    this._then,
  );

  final Fragment$PipelineFields$stages$nodes _instance;

  final TRes Function(Fragment$PipelineFields$stages$nodes) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? name = _undefined,
    Object? status = _undefined,
    Object? detailedStatus = _undefined,
    Object? jobs = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$PipelineFields$stages$nodes(
        name: name == _undefined ? _instance.name : (name as String?),
        status: status == _undefined ? _instance.status : (status as String?),
        detailedStatus: detailedStatus == _undefined
            ? _instance.detailedStatus
            : (detailedStatus
                as Fragment$PipelineFields$stages$nodes$detailedStatus?),
        jobs: jobs == _undefined
            ? _instance.jobs
            : (jobs as Fragment$PipelineFields$stages$nodes$jobs?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Fragment$PipelineFields$stages$nodes$detailedStatus<TRes>
      get detailedStatus {
    final local$detailedStatus = _instance.detailedStatus;
    return local$detailedStatus == null
        ? CopyWith$Fragment$PipelineFields$stages$nodes$detailedStatus.stub(
            _then(_instance))
        : CopyWith$Fragment$PipelineFields$stages$nodes$detailedStatus(
            local$detailedStatus, (e) => call(detailedStatus: e));
  }

  CopyWith$Fragment$PipelineFields$stages$nodes$jobs<TRes> get jobs {
    final local$jobs = _instance.jobs;
    return local$jobs == null
        ? CopyWith$Fragment$PipelineFields$stages$nodes$jobs.stub(
            _then(_instance))
        : CopyWith$Fragment$PipelineFields$stages$nodes$jobs(
            local$jobs, (e) => call(jobs: e));
  }
}

class _CopyWithStubImpl$Fragment$PipelineFields$stages$nodes<TRes>
    implements CopyWith$Fragment$PipelineFields$stages$nodes<TRes> {
  _CopyWithStubImpl$Fragment$PipelineFields$stages$nodes(this._res);

  TRes _res;

  call({
    String? name,
    String? status,
    Fragment$PipelineFields$stages$nodes$detailedStatus? detailedStatus,
    Fragment$PipelineFields$stages$nodes$jobs? jobs,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Fragment$PipelineFields$stages$nodes$detailedStatus<TRes>
      get detailedStatus =>
          CopyWith$Fragment$PipelineFields$stages$nodes$detailedStatus.stub(
              _res);

  CopyWith$Fragment$PipelineFields$stages$nodes$jobs<TRes> get jobs =>
      CopyWith$Fragment$PipelineFields$stages$nodes$jobs.stub(_res);
}

class Fragment$PipelineFields$stages$nodes$detailedStatus {
  Fragment$PipelineFields$stages$nodes$detailedStatus({
    this.icon,
    this.$__typename = 'DetailedStatus',
  });

  factory Fragment$PipelineFields$stages$nodes$detailedStatus.fromJson(
      Map<String, dynamic> json) {
    final l$icon = json['icon'];
    final l$$__typename = json['__typename'];
    return Fragment$PipelineFields$stages$nodes$detailedStatus(
      icon: (l$icon as String?),
      $__typename: (l$$__typename as String),
    );
  }

  @Deprecated(
      'The `icon` attribute is deprecated. Use `name` to identify the status to display instead. Deprecated in 16.4.')
  final String? icon;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$icon = icon;
    _resultData['icon'] = l$icon;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$icon = icon;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$icon,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$PipelineFields$stages$nodes$detailedStatus) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$icon = icon;
    final lOther$icon = other.icon;
    if (l$icon != lOther$icon) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$PipelineFields$stages$nodes$detailedStatus
    on Fragment$PipelineFields$stages$nodes$detailedStatus {
  CopyWith$Fragment$PipelineFields$stages$nodes$detailedStatus<
          Fragment$PipelineFields$stages$nodes$detailedStatus>
      get copyWith =>
          CopyWith$Fragment$PipelineFields$stages$nodes$detailedStatus(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$PipelineFields$stages$nodes$detailedStatus<
    TRes> {
  factory CopyWith$Fragment$PipelineFields$stages$nodes$detailedStatus(
    Fragment$PipelineFields$stages$nodes$detailedStatus instance,
    TRes Function(Fragment$PipelineFields$stages$nodes$detailedStatus) then,
  ) = _CopyWithImpl$Fragment$PipelineFields$stages$nodes$detailedStatus;

  factory CopyWith$Fragment$PipelineFields$stages$nodes$detailedStatus.stub(
          TRes res) =
      _CopyWithStubImpl$Fragment$PipelineFields$stages$nodes$detailedStatus;

  TRes call({
    String? icon,
    String? $__typename,
  });
}

class _CopyWithImpl$Fragment$PipelineFields$stages$nodes$detailedStatus<TRes>
    implements
        CopyWith$Fragment$PipelineFields$stages$nodes$detailedStatus<TRes> {
  _CopyWithImpl$Fragment$PipelineFields$stages$nodes$detailedStatus(
    this._instance,
    this._then,
  );

  final Fragment$PipelineFields$stages$nodes$detailedStatus _instance;

  final TRes Function(Fragment$PipelineFields$stages$nodes$detailedStatus)
      _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? icon = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$PipelineFields$stages$nodes$detailedStatus(
        icon: icon == _undefined ? _instance.icon : (icon as String?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Fragment$PipelineFields$stages$nodes$detailedStatus<
        TRes>
    implements
        CopyWith$Fragment$PipelineFields$stages$nodes$detailedStatus<TRes> {
  _CopyWithStubImpl$Fragment$PipelineFields$stages$nodes$detailedStatus(
      this._res);

  TRes _res;

  call({
    String? icon,
    String? $__typename,
  }) =>
      _res;
}

class Fragment$PipelineFields$stages$nodes$jobs {
  Fragment$PipelineFields$stages$nodes$jobs({
    this.nodes,
    this.$__typename = 'CiJobConnection',
  });

  factory Fragment$PipelineFields$stages$nodes$jobs.fromJson(
      Map<String, dynamic> json) {
    final l$nodes = json['nodes'];
    final l$$__typename = json['__typename'];
    return Fragment$PipelineFields$stages$nodes$jobs(
      nodes: (l$nodes as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Fragment$PipelineFields$stages$nodes$jobs$nodes.fromJson(
                  (e as Map<String, dynamic>)))
          .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final List<Fragment$PipelineFields$stages$nodes$jobs$nodes?>? nodes;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$nodes = nodes;
    _resultData['nodes'] = l$nodes?.map((e) => e?.toJson()).toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$nodes = nodes;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$nodes == null ? null : Object.hashAll(l$nodes.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$PipelineFields$stages$nodes$jobs) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$nodes = nodes;
    final lOther$nodes = other.nodes;
    if (l$nodes != null && lOther$nodes != null) {
      if (l$nodes.length != lOther$nodes.length) {
        return false;
      }
      for (int i = 0; i < l$nodes.length; i++) {
        final l$nodes$entry = l$nodes[i];
        final lOther$nodes$entry = lOther$nodes[i];
        if (l$nodes$entry != lOther$nodes$entry) {
          return false;
        }
      }
    } else if (l$nodes != lOther$nodes) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$PipelineFields$stages$nodes$jobs
    on Fragment$PipelineFields$stages$nodes$jobs {
  CopyWith$Fragment$PipelineFields$stages$nodes$jobs<
          Fragment$PipelineFields$stages$nodes$jobs>
      get copyWith => CopyWith$Fragment$PipelineFields$stages$nodes$jobs(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$PipelineFields$stages$nodes$jobs<TRes> {
  factory CopyWith$Fragment$PipelineFields$stages$nodes$jobs(
    Fragment$PipelineFields$stages$nodes$jobs instance,
    TRes Function(Fragment$PipelineFields$stages$nodes$jobs) then,
  ) = _CopyWithImpl$Fragment$PipelineFields$stages$nodes$jobs;

  factory CopyWith$Fragment$PipelineFields$stages$nodes$jobs.stub(TRes res) =
      _CopyWithStubImpl$Fragment$PipelineFields$stages$nodes$jobs;

  TRes call({
    List<Fragment$PipelineFields$stages$nodes$jobs$nodes?>? nodes,
    String? $__typename,
  });
  TRes nodes(
      Iterable<Fragment$PipelineFields$stages$nodes$jobs$nodes?>? Function(
              Iterable<
                  CopyWith$Fragment$PipelineFields$stages$nodes$jobs$nodes<
                      Fragment$PipelineFields$stages$nodes$jobs$nodes>?>?)
          _fn);
}

class _CopyWithImpl$Fragment$PipelineFields$stages$nodes$jobs<TRes>
    implements CopyWith$Fragment$PipelineFields$stages$nodes$jobs<TRes> {
  _CopyWithImpl$Fragment$PipelineFields$stages$nodes$jobs(
    this._instance,
    this._then,
  );

  final Fragment$PipelineFields$stages$nodes$jobs _instance;

  final TRes Function(Fragment$PipelineFields$stages$nodes$jobs) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? nodes = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$PipelineFields$stages$nodes$jobs(
        nodes: nodes == _undefined
            ? _instance.nodes
            : (nodes
                as List<Fragment$PipelineFields$stages$nodes$jobs$nodes?>?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  TRes nodes(
          Iterable<Fragment$PipelineFields$stages$nodes$jobs$nodes?>? Function(
                  Iterable<
                      CopyWith$Fragment$PipelineFields$stages$nodes$jobs$nodes<
                          Fragment$PipelineFields$stages$nodes$jobs$nodes>?>?)
              _fn) =>
      call(
          nodes: _fn(_instance.nodes?.map((e) => e == null
              ? null
              : CopyWith$Fragment$PipelineFields$stages$nodes$jobs$nodes(
                  e,
                  (i) => i,
                )))?.toList());
}

class _CopyWithStubImpl$Fragment$PipelineFields$stages$nodes$jobs<TRes>
    implements CopyWith$Fragment$PipelineFields$stages$nodes$jobs<TRes> {
  _CopyWithStubImpl$Fragment$PipelineFields$stages$nodes$jobs(this._res);

  TRes _res;

  call({
    List<Fragment$PipelineFields$stages$nodes$jobs$nodes?>? nodes,
    String? $__typename,
  }) =>
      _res;

  nodes(_fn) => _res;
}

class Fragment$PipelineFields$stages$nodes$jobs$nodes {
  Fragment$PipelineFields$stages$nodes$jobs$nodes({
    this.status,
    this.$__typename = 'CiJob',
  });

  factory Fragment$PipelineFields$stages$nodes$jobs$nodes.fromJson(
      Map<String, dynamic> json) {
    final l$status = json['status'];
    final l$$__typename = json['__typename'];
    return Fragment$PipelineFields$stages$nodes$jobs$nodes(
      status: l$status == null
          ? null
          : fromJson$Enum$CiJobStatus((l$status as String)),
      $__typename: (l$$__typename as String),
    );
  }

  final Enum$CiJobStatus? status;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$status = status;
    _resultData['status'] =
        l$status == null ? null : toJson$Enum$CiJobStatus(l$status);
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$status = status;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$status,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$PipelineFields$stages$nodes$jobs$nodes) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$status = status;
    final lOther$status = other.status;
    if (l$status != lOther$status) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$PipelineFields$stages$nodes$jobs$nodes
    on Fragment$PipelineFields$stages$nodes$jobs$nodes {
  CopyWith$Fragment$PipelineFields$stages$nodes$jobs$nodes<
          Fragment$PipelineFields$stages$nodes$jobs$nodes>
      get copyWith => CopyWith$Fragment$PipelineFields$stages$nodes$jobs$nodes(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$PipelineFields$stages$nodes$jobs$nodes<TRes> {
  factory CopyWith$Fragment$PipelineFields$stages$nodes$jobs$nodes(
    Fragment$PipelineFields$stages$nodes$jobs$nodes instance,
    TRes Function(Fragment$PipelineFields$stages$nodes$jobs$nodes) then,
  ) = _CopyWithImpl$Fragment$PipelineFields$stages$nodes$jobs$nodes;

  factory CopyWith$Fragment$PipelineFields$stages$nodes$jobs$nodes.stub(
          TRes res) =
      _CopyWithStubImpl$Fragment$PipelineFields$stages$nodes$jobs$nodes;

  TRes call({
    Enum$CiJobStatus? status,
    String? $__typename,
  });
}

class _CopyWithImpl$Fragment$PipelineFields$stages$nodes$jobs$nodes<TRes>
    implements CopyWith$Fragment$PipelineFields$stages$nodes$jobs$nodes<TRes> {
  _CopyWithImpl$Fragment$PipelineFields$stages$nodes$jobs$nodes(
    this._instance,
    this._then,
  );

  final Fragment$PipelineFields$stages$nodes$jobs$nodes _instance;

  final TRes Function(Fragment$PipelineFields$stages$nodes$jobs$nodes) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? status = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$PipelineFields$stages$nodes$jobs$nodes(
        status: status == _undefined
            ? _instance.status
            : (status as Enum$CiJobStatus?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Fragment$PipelineFields$stages$nodes$jobs$nodes<TRes>
    implements CopyWith$Fragment$PipelineFields$stages$nodes$jobs$nodes<TRes> {
  _CopyWithStubImpl$Fragment$PipelineFields$stages$nodes$jobs$nodes(this._res);

  TRes _res;

  call({
    Enum$CiJobStatus? status,
    String? $__typename,
  }) =>
      _res;
}

class Variables$Query$GroupProjectPipelines {
  factory Variables$Query$GroupProjectPipelines({required String group}) =>
      Variables$Query$GroupProjectPipelines._({
        r'group': group,
      });

  Variables$Query$GroupProjectPipelines._(this._$data);

  factory Variables$Query$GroupProjectPipelines.fromJson(
      Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$group = data['group'];
    result$data['group'] = (l$group as String);
    return Variables$Query$GroupProjectPipelines._(result$data);
  }

  Map<String, dynamic> _$data;

  String get group => (_$data['group'] as String);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$group = group;
    result$data['group'] = l$group;
    return result$data;
  }

  CopyWith$Variables$Query$GroupProjectPipelines<
          Variables$Query$GroupProjectPipelines>
      get copyWith => CopyWith$Variables$Query$GroupProjectPipelines(
            this,
            (i) => i,
          );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Variables$Query$GroupProjectPipelines) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$group = group;
    final lOther$group = other.group;
    if (l$group != lOther$group) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$group = group;
    return Object.hashAll([l$group]);
  }
}

abstract class CopyWith$Variables$Query$GroupProjectPipelines<TRes> {
  factory CopyWith$Variables$Query$GroupProjectPipelines(
    Variables$Query$GroupProjectPipelines instance,
    TRes Function(Variables$Query$GroupProjectPipelines) then,
  ) = _CopyWithImpl$Variables$Query$GroupProjectPipelines;

  factory CopyWith$Variables$Query$GroupProjectPipelines.stub(TRes res) =
      _CopyWithStubImpl$Variables$Query$GroupProjectPipelines;

  TRes call({String? group});
}

class _CopyWithImpl$Variables$Query$GroupProjectPipelines<TRes>
    implements CopyWith$Variables$Query$GroupProjectPipelines<TRes> {
  _CopyWithImpl$Variables$Query$GroupProjectPipelines(
    this._instance,
    this._then,
  );

  final Variables$Query$GroupProjectPipelines _instance;

  final TRes Function(Variables$Query$GroupProjectPipelines) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({Object? group = _undefined}) =>
      _then(Variables$Query$GroupProjectPipelines._({
        ..._instance._$data,
        if (group != _undefined && group != null) 'group': (group as String),
      }));
}

class _CopyWithStubImpl$Variables$Query$GroupProjectPipelines<TRes>
    implements CopyWith$Variables$Query$GroupProjectPipelines<TRes> {
  _CopyWithStubImpl$Variables$Query$GroupProjectPipelines(this._res);

  TRes _res;

  call({String? group}) => _res;
}

class Query$GroupProjectPipelines {
  Query$GroupProjectPipelines({
    this.group,
    this.$__typename = 'Query',
  });

  factory Query$GroupProjectPipelines.fromJson(Map<String, dynamic> json) {
    final l$group = json['group'];
    final l$$__typename = json['__typename'];
    return Query$GroupProjectPipelines(
      group: l$group == null
          ? null
          : Query$GroupProjectPipelines$group.fromJson(
              (l$group as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Query$GroupProjectPipelines$group? group;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$group = group;
    _resultData['group'] = l$group?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$group = group;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$group,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$GroupProjectPipelines) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$group = group;
    final lOther$group = other.group;
    if (l$group != lOther$group) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$GroupProjectPipelines
    on Query$GroupProjectPipelines {
  CopyWith$Query$GroupProjectPipelines<Query$GroupProjectPipelines>
      get copyWith => CopyWith$Query$GroupProjectPipelines(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$GroupProjectPipelines<TRes> {
  factory CopyWith$Query$GroupProjectPipelines(
    Query$GroupProjectPipelines instance,
    TRes Function(Query$GroupProjectPipelines) then,
  ) = _CopyWithImpl$Query$GroupProjectPipelines;

  factory CopyWith$Query$GroupProjectPipelines.stub(TRes res) =
      _CopyWithStubImpl$Query$GroupProjectPipelines;

  TRes call({
    Query$GroupProjectPipelines$group? group,
    String? $__typename,
  });
  CopyWith$Query$GroupProjectPipelines$group<TRes> get group;
}

class _CopyWithImpl$Query$GroupProjectPipelines<TRes>
    implements CopyWith$Query$GroupProjectPipelines<TRes> {
  _CopyWithImpl$Query$GroupProjectPipelines(
    this._instance,
    this._then,
  );

  final Query$GroupProjectPipelines _instance;

  final TRes Function(Query$GroupProjectPipelines) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? group = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$GroupProjectPipelines(
        group: group == _undefined
            ? _instance.group
            : (group as Query$GroupProjectPipelines$group?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Query$GroupProjectPipelines$group<TRes> get group {
    final local$group = _instance.group;
    return local$group == null
        ? CopyWith$Query$GroupProjectPipelines$group.stub(_then(_instance))
        : CopyWith$Query$GroupProjectPipelines$group(
            local$group, (e) => call(group: e));
  }
}

class _CopyWithStubImpl$Query$GroupProjectPipelines<TRes>
    implements CopyWith$Query$GroupProjectPipelines<TRes> {
  _CopyWithStubImpl$Query$GroupProjectPipelines(this._res);

  TRes _res;

  call({
    Query$GroupProjectPipelines$group? group,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Query$GroupProjectPipelines$group<TRes> get group =>
      CopyWith$Query$GroupProjectPipelines$group.stub(_res);
}

const documentNodeQueryGroupProjectPipelines = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.query,
    name: NameNode(value: 'GroupProjectPipelines'),
    variableDefinitions: [
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'group')),
        type: NamedTypeNode(
          name: NameNode(value: 'ID'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      )
    ],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'group'),
        alias: null,
        arguments: [
          ArgumentNode(
            name: NameNode(value: 'fullPath'),
            value: VariableNode(name: NameNode(value: 'group')),
          )
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
            name: NameNode(value: 'projects'),
            alias: null,
            arguments: [
              ArgumentNode(
                name: NameNode(value: 'includeSubgroups'),
                value: BooleanValueNode(value: true),
              )
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                name: NameNode(value: 'nodes'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: SelectionSetNode(selections: [
                  FragmentSpreadNode(
                    name: NameNode(value: 'ProjectFields'),
                    directives: [],
                  ),
                  FieldNode(
                    name: NameNode(value: '__typename'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null,
                  ),
                ]),
              ),
              FieldNode(
                name: NameNode(value: '__typename'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
            ]),
          ),
          FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
        ]),
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
  fragmentDefinitionProjectFields,
  fragmentDefinitionPipelineFields,
]);

class Query$GroupProjectPipelines$group {
  Query$GroupProjectPipelines$group({
    required this.projects,
    this.$__typename = 'Group',
  });

  factory Query$GroupProjectPipelines$group.fromJson(
      Map<String, dynamic> json) {
    final l$projects = json['projects'];
    final l$$__typename = json['__typename'];
    return Query$GroupProjectPipelines$group(
      projects: Query$GroupProjectPipelines$group$projects.fromJson(
          (l$projects as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Query$GroupProjectPipelines$group$projects projects;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$projects = projects;
    _resultData['projects'] = l$projects.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$projects = projects;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$projects,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$GroupProjectPipelines$group) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$projects = projects;
    final lOther$projects = other.projects;
    if (l$projects != lOther$projects) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$GroupProjectPipelines$group
    on Query$GroupProjectPipelines$group {
  CopyWith$Query$GroupProjectPipelines$group<Query$GroupProjectPipelines$group>
      get copyWith => CopyWith$Query$GroupProjectPipelines$group(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$GroupProjectPipelines$group<TRes> {
  factory CopyWith$Query$GroupProjectPipelines$group(
    Query$GroupProjectPipelines$group instance,
    TRes Function(Query$GroupProjectPipelines$group) then,
  ) = _CopyWithImpl$Query$GroupProjectPipelines$group;

  factory CopyWith$Query$GroupProjectPipelines$group.stub(TRes res) =
      _CopyWithStubImpl$Query$GroupProjectPipelines$group;

  TRes call({
    Query$GroupProjectPipelines$group$projects? projects,
    String? $__typename,
  });
  CopyWith$Query$GroupProjectPipelines$group$projects<TRes> get projects;
}

class _CopyWithImpl$Query$GroupProjectPipelines$group<TRes>
    implements CopyWith$Query$GroupProjectPipelines$group<TRes> {
  _CopyWithImpl$Query$GroupProjectPipelines$group(
    this._instance,
    this._then,
  );

  final Query$GroupProjectPipelines$group _instance;

  final TRes Function(Query$GroupProjectPipelines$group) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? projects = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$GroupProjectPipelines$group(
        projects: projects == _undefined || projects == null
            ? _instance.projects
            : (projects as Query$GroupProjectPipelines$group$projects),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Query$GroupProjectPipelines$group$projects<TRes> get projects {
    final local$projects = _instance.projects;
    return CopyWith$Query$GroupProjectPipelines$group$projects(
        local$projects, (e) => call(projects: e));
  }
}

class _CopyWithStubImpl$Query$GroupProjectPipelines$group<TRes>
    implements CopyWith$Query$GroupProjectPipelines$group<TRes> {
  _CopyWithStubImpl$Query$GroupProjectPipelines$group(this._res);

  TRes _res;

  call({
    Query$GroupProjectPipelines$group$projects? projects,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Query$GroupProjectPipelines$group$projects<TRes> get projects =>
      CopyWith$Query$GroupProjectPipelines$group$projects.stub(_res);
}

class Query$GroupProjectPipelines$group$projects {
  Query$GroupProjectPipelines$group$projects({
    this.nodes,
    this.$__typename = 'ProjectConnection',
  });

  factory Query$GroupProjectPipelines$group$projects.fromJson(
      Map<String, dynamic> json) {
    final l$nodes = json['nodes'];
    final l$$__typename = json['__typename'];
    return Query$GroupProjectPipelines$group$projects(
      nodes: (l$nodes as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Fragment$ProjectFields.fromJson((e as Map<String, dynamic>)))
          .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final List<Fragment$ProjectFields?>? nodes;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$nodes = nodes;
    _resultData['nodes'] = l$nodes?.map((e) => e?.toJson()).toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$nodes = nodes;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$nodes == null ? null : Object.hashAll(l$nodes.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$GroupProjectPipelines$group$projects) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$nodes = nodes;
    final lOther$nodes = other.nodes;
    if (l$nodes != null && lOther$nodes != null) {
      if (l$nodes.length != lOther$nodes.length) {
        return false;
      }
      for (int i = 0; i < l$nodes.length; i++) {
        final l$nodes$entry = l$nodes[i];
        final lOther$nodes$entry = lOther$nodes[i];
        if (l$nodes$entry != lOther$nodes$entry) {
          return false;
        }
      }
    } else if (l$nodes != lOther$nodes) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$GroupProjectPipelines$group$projects
    on Query$GroupProjectPipelines$group$projects {
  CopyWith$Query$GroupProjectPipelines$group$projects<
          Query$GroupProjectPipelines$group$projects>
      get copyWith => CopyWith$Query$GroupProjectPipelines$group$projects(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$GroupProjectPipelines$group$projects<TRes> {
  factory CopyWith$Query$GroupProjectPipelines$group$projects(
    Query$GroupProjectPipelines$group$projects instance,
    TRes Function(Query$GroupProjectPipelines$group$projects) then,
  ) = _CopyWithImpl$Query$GroupProjectPipelines$group$projects;

  factory CopyWith$Query$GroupProjectPipelines$group$projects.stub(TRes res) =
      _CopyWithStubImpl$Query$GroupProjectPipelines$group$projects;

  TRes call({
    List<Fragment$ProjectFields?>? nodes,
    String? $__typename,
  });
  TRes nodes(
      Iterable<Fragment$ProjectFields?>? Function(
              Iterable<
                  CopyWith$Fragment$ProjectFields<Fragment$ProjectFields>?>?)
          _fn);
}

class _CopyWithImpl$Query$GroupProjectPipelines$group$projects<TRes>
    implements CopyWith$Query$GroupProjectPipelines$group$projects<TRes> {
  _CopyWithImpl$Query$GroupProjectPipelines$group$projects(
    this._instance,
    this._then,
  );

  final Query$GroupProjectPipelines$group$projects _instance;

  final TRes Function(Query$GroupProjectPipelines$group$projects) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? nodes = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$GroupProjectPipelines$group$projects(
        nodes: nodes == _undefined
            ? _instance.nodes
            : (nodes as List<Fragment$ProjectFields?>?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  TRes nodes(
          Iterable<Fragment$ProjectFields?>? Function(
                  Iterable<
                      CopyWith$Fragment$ProjectFields<
                          Fragment$ProjectFields>?>?)
              _fn) =>
      call(
          nodes: _fn(_instance.nodes?.map((e) => e == null
              ? null
              : CopyWith$Fragment$ProjectFields(
                  e,
                  (i) => i,
                )))?.toList());
}

class _CopyWithStubImpl$Query$GroupProjectPipelines$group$projects<TRes>
    implements CopyWith$Query$GroupProjectPipelines$group$projects<TRes> {
  _CopyWithStubImpl$Query$GroupProjectPipelines$group$projects(this._res);

  TRes _res;

  call({
    List<Fragment$ProjectFields?>? nodes,
    String? $__typename,
  }) =>
      _res;

  nodes(_fn) => _res;
}

class Variables$Query$ProjectPipelines {
  factory Variables$Query$ProjectPipelines({required String project}) =>
      Variables$Query$ProjectPipelines._({
        r'project': project,
      });

  Variables$Query$ProjectPipelines._(this._$data);

  factory Variables$Query$ProjectPipelines.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$project = data['project'];
    result$data['project'] = (l$project as String);
    return Variables$Query$ProjectPipelines._(result$data);
  }

  Map<String, dynamic> _$data;

  String get project => (_$data['project'] as String);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$project = project;
    result$data['project'] = l$project;
    return result$data;
  }

  CopyWith$Variables$Query$ProjectPipelines<Variables$Query$ProjectPipelines>
      get copyWith => CopyWith$Variables$Query$ProjectPipelines(
            this,
            (i) => i,
          );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Variables$Query$ProjectPipelines) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$project = project;
    final lOther$project = other.project;
    if (l$project != lOther$project) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$project = project;
    return Object.hashAll([l$project]);
  }
}

abstract class CopyWith$Variables$Query$ProjectPipelines<TRes> {
  factory CopyWith$Variables$Query$ProjectPipelines(
    Variables$Query$ProjectPipelines instance,
    TRes Function(Variables$Query$ProjectPipelines) then,
  ) = _CopyWithImpl$Variables$Query$ProjectPipelines;

  factory CopyWith$Variables$Query$ProjectPipelines.stub(TRes res) =
      _CopyWithStubImpl$Variables$Query$ProjectPipelines;

  TRes call({String? project});
}

class _CopyWithImpl$Variables$Query$ProjectPipelines<TRes>
    implements CopyWith$Variables$Query$ProjectPipelines<TRes> {
  _CopyWithImpl$Variables$Query$ProjectPipelines(
    this._instance,
    this._then,
  );

  final Variables$Query$ProjectPipelines _instance;

  final TRes Function(Variables$Query$ProjectPipelines) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({Object? project = _undefined}) =>
      _then(Variables$Query$ProjectPipelines._({
        ..._instance._$data,
        if (project != _undefined && project != null)
          'project': (project as String),
      }));
}

class _CopyWithStubImpl$Variables$Query$ProjectPipelines<TRes>
    implements CopyWith$Variables$Query$ProjectPipelines<TRes> {
  _CopyWithStubImpl$Variables$Query$ProjectPipelines(this._res);

  TRes _res;

  call({String? project}) => _res;
}

class Query$ProjectPipelines {
  Query$ProjectPipelines({
    this.project,
    this.$__typename = 'Query',
  });

  factory Query$ProjectPipelines.fromJson(Map<String, dynamic> json) {
    final l$project = json['project'];
    final l$$__typename = json['__typename'];
    return Query$ProjectPipelines(
      project: l$project == null
          ? null
          : Fragment$ProjectFields.fromJson(
              (l$project as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Fragment$ProjectFields? project;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$project = project;
    _resultData['project'] = l$project?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$project = project;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$project,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$ProjectPipelines) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$project = project;
    final lOther$project = other.project;
    if (l$project != lOther$project) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$ProjectPipelines on Query$ProjectPipelines {
  CopyWith$Query$ProjectPipelines<Query$ProjectPipelines> get copyWith =>
      CopyWith$Query$ProjectPipelines(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Query$ProjectPipelines<TRes> {
  factory CopyWith$Query$ProjectPipelines(
    Query$ProjectPipelines instance,
    TRes Function(Query$ProjectPipelines) then,
  ) = _CopyWithImpl$Query$ProjectPipelines;

  factory CopyWith$Query$ProjectPipelines.stub(TRes res) =
      _CopyWithStubImpl$Query$ProjectPipelines;

  TRes call({
    Fragment$ProjectFields? project,
    String? $__typename,
  });
  CopyWith$Fragment$ProjectFields<TRes> get project;
}

class _CopyWithImpl$Query$ProjectPipelines<TRes>
    implements CopyWith$Query$ProjectPipelines<TRes> {
  _CopyWithImpl$Query$ProjectPipelines(
    this._instance,
    this._then,
  );

  final Query$ProjectPipelines _instance;

  final TRes Function(Query$ProjectPipelines) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? project = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$ProjectPipelines(
        project: project == _undefined
            ? _instance.project
            : (project as Fragment$ProjectFields?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Fragment$ProjectFields<TRes> get project {
    final local$project = _instance.project;
    return local$project == null
        ? CopyWith$Fragment$ProjectFields.stub(_then(_instance))
        : CopyWith$Fragment$ProjectFields(
            local$project, (e) => call(project: e));
  }
}

class _CopyWithStubImpl$Query$ProjectPipelines<TRes>
    implements CopyWith$Query$ProjectPipelines<TRes> {
  _CopyWithStubImpl$Query$ProjectPipelines(this._res);

  TRes _res;

  call({
    Fragment$ProjectFields? project,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Fragment$ProjectFields<TRes> get project =>
      CopyWith$Fragment$ProjectFields.stub(_res);
}

const documentNodeQueryProjectPipelines = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.query,
    name: NameNode(value: 'ProjectPipelines'),
    variableDefinitions: [
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'project')),
        type: NamedTypeNode(
          name: NameNode(value: 'ID'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      )
    ],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'project'),
        alias: null,
        arguments: [
          ArgumentNode(
            name: NameNode(value: 'fullPath'),
            value: VariableNode(name: NameNode(value: 'project')),
          )
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FragmentSpreadNode(
            name: NameNode(value: 'ProjectFields'),
            directives: [],
          ),
          FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
        ]),
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
  fragmentDefinitionProjectFields,
  fragmentDefinitionPipelineFields,
]);

class Query$CurrentUserPipelines {
  Query$CurrentUserPipelines({
    this.user,
    this.$__typename = 'Query',
  });

  factory Query$CurrentUserPipelines.fromJson(Map<String, dynamic> json) {
    final l$user = json['user'];
    final l$$__typename = json['__typename'];
    return Query$CurrentUserPipelines(
      user: l$user == null
          ? null
          : Query$CurrentUserPipelines$user.fromJson(
              (l$user as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Query$CurrentUserPipelines$user? user;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$user = user;
    _resultData['user'] = l$user?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$user = user;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$user,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$CurrentUserPipelines) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$user = user;
    final lOther$user = other.user;
    if (l$user != lOther$user) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$CurrentUserPipelines
    on Query$CurrentUserPipelines {
  CopyWith$Query$CurrentUserPipelines<Query$CurrentUserPipelines>
      get copyWith => CopyWith$Query$CurrentUserPipelines(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$CurrentUserPipelines<TRes> {
  factory CopyWith$Query$CurrentUserPipelines(
    Query$CurrentUserPipelines instance,
    TRes Function(Query$CurrentUserPipelines) then,
  ) = _CopyWithImpl$Query$CurrentUserPipelines;

  factory CopyWith$Query$CurrentUserPipelines.stub(TRes res) =
      _CopyWithStubImpl$Query$CurrentUserPipelines;

  TRes call({
    Query$CurrentUserPipelines$user? user,
    String? $__typename,
  });
  CopyWith$Query$CurrentUserPipelines$user<TRes> get user;
}

class _CopyWithImpl$Query$CurrentUserPipelines<TRes>
    implements CopyWith$Query$CurrentUserPipelines<TRes> {
  _CopyWithImpl$Query$CurrentUserPipelines(
    this._instance,
    this._then,
  );

  final Query$CurrentUserPipelines _instance;

  final TRes Function(Query$CurrentUserPipelines) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? user = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$CurrentUserPipelines(
        user: user == _undefined
            ? _instance.user
            : (user as Query$CurrentUserPipelines$user?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Query$CurrentUserPipelines$user<TRes> get user {
    final local$user = _instance.user;
    return local$user == null
        ? CopyWith$Query$CurrentUserPipelines$user.stub(_then(_instance))
        : CopyWith$Query$CurrentUserPipelines$user(
            local$user, (e) => call(user: e));
  }
}

class _CopyWithStubImpl$Query$CurrentUserPipelines<TRes>
    implements CopyWith$Query$CurrentUserPipelines<TRes> {
  _CopyWithStubImpl$Query$CurrentUserPipelines(this._res);

  TRes _res;

  call({
    Query$CurrentUserPipelines$user? user,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Query$CurrentUserPipelines$user<TRes> get user =>
      CopyWith$Query$CurrentUserPipelines$user.stub(_res);
}

const documentNodeQueryCurrentUserPipelines = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.query,
    name: NameNode(value: 'CurrentUserPipelines'),
    variableDefinitions: [],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'currentUser'),
        alias: NameNode(value: 'user'),
        arguments: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
            name: NameNode(value: 'username'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
          FragmentSpreadNode(
            name: NameNode(value: 'UserProjects'),
            directives: [],
          ),
          FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
        ]),
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
  fragmentDefinitionUserProjects,
  fragmentDefinitionProjectFields,
  fragmentDefinitionPipelineFields,
]);

class Query$CurrentUserPipelines$user implements Fragment$UserProjects {
  Query$CurrentUserPipelines$user({
    required this.username,
    this.projectMemberships,
    this.$__typename = 'CurrentUser',
  });

  factory Query$CurrentUserPipelines$user.fromJson(Map<String, dynamic> json) {
    final l$username = json['username'];
    final l$projectMemberships = json['projectMemberships'];
    final l$$__typename = json['__typename'];
    return Query$CurrentUserPipelines$user(
      username: (l$username as String),
      projectMemberships: l$projectMemberships == null
          ? null
          : Query$CurrentUserPipelines$user$projectMemberships.fromJson(
              (l$projectMemberships as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final String username;

  final Query$CurrentUserPipelines$user$projectMemberships? projectMemberships;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$username = username;
    _resultData['username'] = l$username;
    final l$projectMemberships = projectMemberships;
    _resultData['projectMemberships'] = l$projectMemberships?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$username = username;
    final l$projectMemberships = projectMemberships;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$username,
      l$projectMemberships,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$CurrentUserPipelines$user) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$username = username;
    final lOther$username = other.username;
    if (l$username != lOther$username) {
      return false;
    }
    final l$projectMemberships = projectMemberships;
    final lOther$projectMemberships = other.projectMemberships;
    if (l$projectMemberships != lOther$projectMemberships) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$CurrentUserPipelines$user
    on Query$CurrentUserPipelines$user {
  CopyWith$Query$CurrentUserPipelines$user<Query$CurrentUserPipelines$user>
      get copyWith => CopyWith$Query$CurrentUserPipelines$user(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$CurrentUserPipelines$user<TRes> {
  factory CopyWith$Query$CurrentUserPipelines$user(
    Query$CurrentUserPipelines$user instance,
    TRes Function(Query$CurrentUserPipelines$user) then,
  ) = _CopyWithImpl$Query$CurrentUserPipelines$user;

  factory CopyWith$Query$CurrentUserPipelines$user.stub(TRes res) =
      _CopyWithStubImpl$Query$CurrentUserPipelines$user;

  TRes call({
    String? username,
    Query$CurrentUserPipelines$user$projectMemberships? projectMemberships,
    String? $__typename,
  });
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships<TRes>
      get projectMemberships;
}

class _CopyWithImpl$Query$CurrentUserPipelines$user<TRes>
    implements CopyWith$Query$CurrentUserPipelines$user<TRes> {
  _CopyWithImpl$Query$CurrentUserPipelines$user(
    this._instance,
    this._then,
  );

  final Query$CurrentUserPipelines$user _instance;

  final TRes Function(Query$CurrentUserPipelines$user) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? username = _undefined,
    Object? projectMemberships = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$CurrentUserPipelines$user(
        username: username == _undefined || username == null
            ? _instance.username
            : (username as String),
        projectMemberships: projectMemberships == _undefined
            ? _instance.projectMemberships
            : (projectMemberships
                as Query$CurrentUserPipelines$user$projectMemberships?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Query$CurrentUserPipelines$user$projectMemberships<TRes>
      get projectMemberships {
    final local$projectMemberships = _instance.projectMemberships;
    return local$projectMemberships == null
        ? CopyWith$Query$CurrentUserPipelines$user$projectMemberships.stub(
            _then(_instance))
        : CopyWith$Query$CurrentUserPipelines$user$projectMemberships(
            local$projectMemberships, (e) => call(projectMemberships: e));
  }
}

class _CopyWithStubImpl$Query$CurrentUserPipelines$user<TRes>
    implements CopyWith$Query$CurrentUserPipelines$user<TRes> {
  _CopyWithStubImpl$Query$CurrentUserPipelines$user(this._res);

  TRes _res;

  call({
    String? username,
    Query$CurrentUserPipelines$user$projectMemberships? projectMemberships,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Query$CurrentUserPipelines$user$projectMemberships<TRes>
      get projectMemberships =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships.stub(
              _res);
}

class Query$CurrentUserPipelines$user$projectMemberships
    implements Fragment$UserProjects$projectMemberships {
  Query$CurrentUserPipelines$user$projectMemberships({
    this.nodes,
    this.$__typename = 'ProjectMemberConnection',
  });

  factory Query$CurrentUserPipelines$user$projectMemberships.fromJson(
      Map<String, dynamic> json) {
    final l$nodes = json['nodes'];
    final l$$__typename = json['__typename'];
    return Query$CurrentUserPipelines$user$projectMemberships(
      nodes: (l$nodes as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Query$CurrentUserPipelines$user$projectMemberships$nodes
                  .fromJson((e as Map<String, dynamic>)))
          .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final List<Query$CurrentUserPipelines$user$projectMemberships$nodes?>? nodes;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$nodes = nodes;
    _resultData['nodes'] = l$nodes?.map((e) => e?.toJson()).toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$nodes = nodes;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$nodes == null ? null : Object.hashAll(l$nodes.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$CurrentUserPipelines$user$projectMemberships) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$nodes = nodes;
    final lOther$nodes = other.nodes;
    if (l$nodes != null && lOther$nodes != null) {
      if (l$nodes.length != lOther$nodes.length) {
        return false;
      }
      for (int i = 0; i < l$nodes.length; i++) {
        final l$nodes$entry = l$nodes[i];
        final lOther$nodes$entry = lOther$nodes[i];
        if (l$nodes$entry != lOther$nodes$entry) {
          return false;
        }
      }
    } else if (l$nodes != lOther$nodes) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$CurrentUserPipelines$user$projectMemberships
    on Query$CurrentUserPipelines$user$projectMemberships {
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships<
          Query$CurrentUserPipelines$user$projectMemberships>
      get copyWith =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$CurrentUserPipelines$user$projectMemberships<
    TRes> {
  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships(
    Query$CurrentUserPipelines$user$projectMemberships instance,
    TRes Function(Query$CurrentUserPipelines$user$projectMemberships) then,
  ) = _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships;

  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships.stub(
          TRes res) =
      _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships;

  TRes call({
    List<Query$CurrentUserPipelines$user$projectMemberships$nodes?>? nodes,
    String? $__typename,
  });
  TRes nodes(
      Iterable<Query$CurrentUserPipelines$user$projectMemberships$nodes?>? Function(
              Iterable<
                  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes<
                      Query$CurrentUserPipelines$user$projectMemberships$nodes>?>?)
          _fn);
}

class _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships<TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships<TRes> {
  _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships(
    this._instance,
    this._then,
  );

  final Query$CurrentUserPipelines$user$projectMemberships _instance;

  final TRes Function(Query$CurrentUserPipelines$user$projectMemberships) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? nodes = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$CurrentUserPipelines$user$projectMemberships(
        nodes: nodes == _undefined
            ? _instance.nodes
            : (nodes as List<
                Query$CurrentUserPipelines$user$projectMemberships$nodes?>?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  TRes nodes(
          Iterable<Query$CurrentUserPipelines$user$projectMemberships$nodes?>? Function(
                  Iterable<
                      CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes<
                          Query$CurrentUserPipelines$user$projectMemberships$nodes>?>?)
              _fn) =>
      call(
          nodes: _fn(_instance.nodes?.map((e) => e == null
              ? null
              : CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes(
                  e,
                  (i) => i,
                )))?.toList());
}

class _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships<TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships<TRes> {
  _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships(
      this._res);

  TRes _res;

  call({
    List<Query$CurrentUserPipelines$user$projectMemberships$nodes?>? nodes,
    String? $__typename,
  }) =>
      _res;

  nodes(_fn) => _res;
}

class Query$CurrentUserPipelines$user$projectMemberships$nodes
    implements Fragment$UserProjects$projectMemberships$nodes {
  Query$CurrentUserPipelines$user$projectMemberships$nodes({
    this.project,
    this.$__typename = 'ProjectMember',
  });

  factory Query$CurrentUserPipelines$user$projectMemberships$nodes.fromJson(
      Map<String, dynamic> json) {
    final l$project = json['project'];
    final l$$__typename = json['__typename'];
    return Query$CurrentUserPipelines$user$projectMemberships$nodes(
      project: l$project == null
          ? null
          : Query$CurrentUserPipelines$user$projectMemberships$nodes$project
              .fromJson((l$project as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Query$CurrentUserPipelines$user$projectMemberships$nodes$project?
      project;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$project = project;
    _resultData['project'] = l$project?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$project = project;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$project,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$CurrentUserPipelines$user$projectMemberships$nodes) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$project = project;
    final lOther$project = other.project;
    if (l$project != lOther$project) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$CurrentUserPipelines$user$projectMemberships$nodes
    on Query$CurrentUserPipelines$user$projectMemberships$nodes {
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes<
          Query$CurrentUserPipelines$user$projectMemberships$nodes>
      get copyWith =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes<
    TRes> {
  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes(
    Query$CurrentUserPipelines$user$projectMemberships$nodes instance,
    TRes Function(Query$CurrentUserPipelines$user$projectMemberships$nodes)
        then,
  ) = _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes;

  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes.stub(
          TRes res) =
      _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes;

  TRes call({
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project? project,
    String? $__typename,
  });
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project<
      TRes> get project;
}

class _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes<
        TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes<
            TRes> {
  _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes(
    this._instance,
    this._then,
  );

  final Query$CurrentUserPipelines$user$projectMemberships$nodes _instance;

  final TRes Function(Query$CurrentUserPipelines$user$projectMemberships$nodes)
      _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? project = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$CurrentUserPipelines$user$projectMemberships$nodes(
        project: project == _undefined
            ? _instance.project
            : (project
                as Query$CurrentUserPipelines$user$projectMemberships$nodes$project?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project<
      TRes> get project {
    final local$project = _instance.project;
    return local$project == null
        ? CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project
            .stub(_then(_instance))
        : CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project(
            local$project, (e) => call(project: e));
  }
}

class _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes<
        TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes<
            TRes> {
  _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes(
      this._res);

  TRes _res;

  call({
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project? project,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project<
          TRes>
      get project =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project
              .stub(_res);
}

class Query$CurrentUserPipelines$user$projectMemberships$nodes$project
    implements
        Fragment$UserProjects$projectMemberships$nodes$project,
        Fragment$ProjectFields {
  Query$CurrentUserPipelines$user$projectMemberships$nodes$project({
    this.group,
    this.namespace,
    required this.name,
    required this.fullPath,
    this.repository,
    this.pipelines,
    this.tagPipelines,
    this.$__typename = 'Project',
  });

  factory Query$CurrentUserPipelines$user$projectMemberships$nodes$project.fromJson(
      Map<String, dynamic> json) {
    final l$group = json['group'];
    final l$namespace = json['namespace'];
    final l$name = json['name'];
    final l$fullPath = json['fullPath'];
    final l$repository = json['repository'];
    final l$pipelines = json['pipelines'];
    final l$tagPipelines = json['tagPipelines'];
    final l$$__typename = json['__typename'];
    return Query$CurrentUserPipelines$user$projectMemberships$nodes$project(
      group: l$group == null
          ? null
          : Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group
              .fromJson((l$group as Map<String, dynamic>)),
      namespace: l$namespace == null
          ? null
          : Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace
              .fromJson((l$namespace as Map<String, dynamic>)),
      name: (l$name as String),
      fullPath: (l$fullPath as String),
      repository: l$repository == null
          ? null
          : Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository
              .fromJson((l$repository as Map<String, dynamic>)),
      pipelines: l$pipelines == null
          ? null
          : Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines
              .fromJson((l$pipelines as Map<String, dynamic>)),
      tagPipelines: l$tagPipelines == null
          ? null
          : Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines
              .fromJson((l$tagPipelines as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group?
      group;

  final Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace?
      namespace;

  final String name;

  final String fullPath;

  final Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository?
      repository;

  final Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines?
      pipelines;

  final Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines?
      tagPipelines;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$group = group;
    _resultData['group'] = l$group?.toJson();
    final l$namespace = namespace;
    _resultData['namespace'] = l$namespace?.toJson();
    final l$name = name;
    _resultData['name'] = l$name;
    final l$fullPath = fullPath;
    _resultData['fullPath'] = l$fullPath;
    final l$repository = repository;
    _resultData['repository'] = l$repository?.toJson();
    final l$pipelines = pipelines;
    _resultData['pipelines'] = l$pipelines?.toJson();
    final l$tagPipelines = tagPipelines;
    _resultData['tagPipelines'] = l$tagPipelines?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$group = group;
    final l$namespace = namespace;
    final l$name = name;
    final l$fullPath = fullPath;
    final l$repository = repository;
    final l$pipelines = pipelines;
    final l$tagPipelines = tagPipelines;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$group,
      l$namespace,
      l$name,
      l$fullPath,
      l$repository,
      l$pipelines,
      l$tagPipelines,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other
            is Query$CurrentUserPipelines$user$projectMemberships$nodes$project) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$group = group;
    final lOther$group = other.group;
    if (l$group != lOther$group) {
      return false;
    }
    final l$namespace = namespace;
    final lOther$namespace = other.namespace;
    if (l$namespace != lOther$namespace) {
      return false;
    }
    final l$name = name;
    final lOther$name = other.name;
    if (l$name != lOther$name) {
      return false;
    }
    final l$fullPath = fullPath;
    final lOther$fullPath = other.fullPath;
    if (l$fullPath != lOther$fullPath) {
      return false;
    }
    final l$repository = repository;
    final lOther$repository = other.repository;
    if (l$repository != lOther$repository) {
      return false;
    }
    final l$pipelines = pipelines;
    final lOther$pipelines = other.pipelines;
    if (l$pipelines != lOther$pipelines) {
      return false;
    }
    final l$tagPipelines = tagPipelines;
    final lOther$tagPipelines = other.tagPipelines;
    if (l$tagPipelines != lOther$tagPipelines) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$CurrentUserPipelines$user$projectMemberships$nodes$project
    on Query$CurrentUserPipelines$user$projectMemberships$nodes$project {
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project<
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project>
      get copyWith =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project<
    TRes> {
  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project(
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project instance,
    TRes Function(
            Query$CurrentUserPipelines$user$projectMemberships$nodes$project)
        then,
  ) = _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project;

  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project.stub(
          TRes res) =
      _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project;

  TRes call({
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group?
        group,
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace?
        namespace,
    String? name,
    String? fullPath,
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository?
        repository,
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines?
        pipelines,
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines?
        tagPipelines,
    String? $__typename,
  });
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group<
      TRes> get group;
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace<
      TRes> get namespace;
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository<
      TRes> get repository;
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines<
      TRes> get pipelines;
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines<
      TRes> get tagPipelines;
}

class _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project<
        TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project<
            TRes> {
  _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project(
    this._instance,
    this._then,
  );

  final Query$CurrentUserPipelines$user$projectMemberships$nodes$project
      _instance;

  final TRes Function(
      Query$CurrentUserPipelines$user$projectMemberships$nodes$project) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? group = _undefined,
    Object? namespace = _undefined,
    Object? name = _undefined,
    Object? fullPath = _undefined,
    Object? repository = _undefined,
    Object? pipelines = _undefined,
    Object? tagPipelines = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$CurrentUserPipelines$user$projectMemberships$nodes$project(
        group: group == _undefined
            ? _instance.group
            : (group
                as Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group?),
        namespace: namespace == _undefined
            ? _instance.namespace
            : (namespace
                as Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace?),
        name: name == _undefined || name == null
            ? _instance.name
            : (name as String),
        fullPath: fullPath == _undefined || fullPath == null
            ? _instance.fullPath
            : (fullPath as String),
        repository: repository == _undefined
            ? _instance.repository
            : (repository
                as Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository?),
        pipelines: pipelines == _undefined
            ? _instance.pipelines
            : (pipelines
                as Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines?),
        tagPipelines: tagPipelines == _undefined
            ? _instance.tagPipelines
            : (tagPipelines
                as Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group<
      TRes> get group {
    final local$group = _instance.group;
    return local$group == null
        ? CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group
            .stub(_then(_instance))
        : CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group(
            local$group, (e) => call(group: e));
  }

  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace<
      TRes> get namespace {
    final local$namespace = _instance.namespace;
    return local$namespace == null
        ? CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace
            .stub(_then(_instance))
        : CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace(
            local$namespace, (e) => call(namespace: e));
  }

  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository<
      TRes> get repository {
    final local$repository = _instance.repository;
    return local$repository == null
        ? CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository
            .stub(_then(_instance))
        : CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository(
            local$repository, (e) => call(repository: e));
  }

  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines<
      TRes> get pipelines {
    final local$pipelines = _instance.pipelines;
    return local$pipelines == null
        ? CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines
            .stub(_then(_instance))
        : CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines(
            local$pipelines, (e) => call(pipelines: e));
  }

  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines<
      TRes> get tagPipelines {
    final local$tagPipelines = _instance.tagPipelines;
    return local$tagPipelines == null
        ? CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines
            .stub(_then(_instance))
        : CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines(
            local$tagPipelines, (e) => call(tagPipelines: e));
  }
}

class _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project<
        TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project<
            TRes> {
  _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project(
      this._res);

  TRes _res;

  call({
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group?
        group,
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace?
        namespace,
    String? name,
    String? fullPath,
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository?
        repository,
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines?
        pipelines,
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines?
        tagPipelines,
    String? $__typename,
  }) =>
      _res;

  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group<
          TRes>
      get group =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group
              .stub(_res);

  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace<
          TRes>
      get namespace =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace
              .stub(_res);

  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository<
          TRes>
      get repository =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository
              .stub(_res);

  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines<
          TRes>
      get pipelines =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines
              .stub(_res);

  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines<
          TRes>
      get tagPipelines =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines
              .stub(_res);
}

class Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group
    implements Fragment$UserProjects$projectMemberships$nodes$project$group {
  Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group({
    required this.name,
    this.$__typename = 'Group',
  });

  factory Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group.fromJson(
      Map<String, dynamic> json) {
    final l$name = json['name'];
    final l$$__typename = json['__typename'];
    return Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group(
      name: (l$name as String),
      $__typename: (l$$__typename as String),
    );
  }

  final String name;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$name = name;
    _resultData['name'] = l$name;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$name = name;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$name,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other
            is Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$name = name;
    final lOther$name = other.name;
    if (l$name != lOther$name) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group
    on Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group {
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group<
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group>
      get copyWith =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group<
    TRes> {
  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group(
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group
        instance,
    TRes Function(
            Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group)
        then,
  ) = _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group;

  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group.stub(
          TRes res) =
      _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group;

  TRes call({
    String? name,
    String? $__typename,
  });
}

class _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group<
        TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group<
            TRes> {
  _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group(
    this._instance,
    this._then,
  );

  final Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group
      _instance;

  final TRes Function(
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group)
      _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? name = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group(
        name: name == _undefined || name == null
            ? _instance.name
            : (name as String),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group<
        TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group<
            TRes> {
  _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$group(
      this._res);

  TRes _res;

  call({
    String? name,
    String? $__typename,
  }) =>
      _res;
}

class Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace
    implements
        Fragment$UserProjects$projectMemberships$nodes$project$namespace {
  Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace({
    required this.fullPath,
    this.$__typename = 'Namespace',
  });

  factory Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace.fromJson(
      Map<String, dynamic> json) {
    final l$fullPath = json['fullPath'];
    final l$$__typename = json['__typename'];
    return Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace(
      fullPath: (l$fullPath as String),
      $__typename: (l$$__typename as String),
    );
  }

  final String fullPath;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$fullPath = fullPath;
    _resultData['fullPath'] = l$fullPath;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$fullPath = fullPath;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$fullPath,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other
            is Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$fullPath = fullPath;
    final lOther$fullPath = other.fullPath;
    if (l$fullPath != lOther$fullPath) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace
    on Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace {
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace<
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace>
      get copyWith =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace<
    TRes> {
  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace(
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace
        instance,
    TRes Function(
            Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace)
        then,
  ) = _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace;

  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace.stub(
          TRes res) =
      _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace;

  TRes call({
    String? fullPath,
    String? $__typename,
  });
}

class _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace<
        TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace<
            TRes> {
  _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace(
    this._instance,
    this._then,
  );

  final Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace
      _instance;

  final TRes Function(
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace)
      _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? fullPath = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace(
        fullPath: fullPath == _undefined || fullPath == null
            ? _instance.fullPath
            : (fullPath as String),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace<
        TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace<
            TRes> {
  _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$namespace(
      this._res);

  TRes _res;

  call({
    String? fullPath,
    String? $__typename,
  }) =>
      _res;
}

class Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository
    implements
        Fragment$UserProjects$projectMemberships$nodes$project$repository,
        Fragment$ProjectFields$repository {
  Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository({
    this.defaultBranch,
    this.$__typename = 'Repository',
  });

  factory Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository.fromJson(
      Map<String, dynamic> json) {
    final l$defaultBranch = json['defaultBranch'];
    final l$$__typename = json['__typename'];
    return Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository(
      defaultBranch: (l$defaultBranch as String?),
      $__typename: (l$$__typename as String),
    );
  }

  final String? defaultBranch;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$defaultBranch = defaultBranch;
    _resultData['defaultBranch'] = l$defaultBranch;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$defaultBranch = defaultBranch;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$defaultBranch,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other
            is Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$defaultBranch = defaultBranch;
    final lOther$defaultBranch = other.defaultBranch;
    if (l$defaultBranch != lOther$defaultBranch) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository
    on Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository {
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository<
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository>
      get copyWith =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository<
    TRes> {
  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository(
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository
        instance,
    TRes Function(
            Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository)
        then,
  ) = _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository;

  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository.stub(
          TRes res) =
      _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository;

  TRes call({
    String? defaultBranch,
    String? $__typename,
  });
}

class _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository<
        TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository<
            TRes> {
  _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository(
    this._instance,
    this._then,
  );

  final Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository
      _instance;

  final TRes Function(
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository)
      _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? defaultBranch = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository(
        defaultBranch: defaultBranch == _undefined
            ? _instance.defaultBranch
            : (defaultBranch as String?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository<
        TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository<
            TRes> {
  _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$repository(
      this._res);

  TRes _res;

  call({
    String? defaultBranch,
    String? $__typename,
  }) =>
      _res;
}

class Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines
    implements
        Fragment$UserProjects$projectMemberships$nodes$project$pipelines,
        Fragment$ProjectFields$pipelines {
  Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines({
    this.nodes,
    this.$__typename = 'PipelineConnection',
  });

  factory Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines.fromJson(
      Map<String, dynamic> json) {
    final l$nodes = json['nodes'];
    final l$$__typename = json['__typename'];
    return Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines(
      nodes: (l$nodes as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Fragment$PipelineFields.fromJson((e as Map<String, dynamic>)))
          .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final List<Fragment$PipelineFields?>? nodes;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$nodes = nodes;
    _resultData['nodes'] = l$nodes?.map((e) => e?.toJson()).toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$nodes = nodes;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$nodes == null ? null : Object.hashAll(l$nodes.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other
            is Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$nodes = nodes;
    final lOther$nodes = other.nodes;
    if (l$nodes != null && lOther$nodes != null) {
      if (l$nodes.length != lOther$nodes.length) {
        return false;
      }
      for (int i = 0; i < l$nodes.length; i++) {
        final l$nodes$entry = l$nodes[i];
        final lOther$nodes$entry = lOther$nodes[i];
        if (l$nodes$entry != lOther$nodes$entry) {
          return false;
        }
      }
    } else if (l$nodes != lOther$nodes) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines
    on Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines {
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines<
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines>
      get copyWith =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines<
    TRes> {
  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines(
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines
        instance,
    TRes Function(
            Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines)
        then,
  ) = _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines;

  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines.stub(
          TRes res) =
      _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines;

  TRes call({
    List<Fragment$PipelineFields?>? nodes,
    String? $__typename,
  });
  TRes nodes(
      Iterable<Fragment$PipelineFields?>? Function(
              Iterable<
                  CopyWith$Fragment$PipelineFields<Fragment$PipelineFields>?>?)
          _fn);
}

class _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines<
        TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines<
            TRes> {
  _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines(
    this._instance,
    this._then,
  );

  final Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines
      _instance;

  final TRes Function(
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines)
      _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? nodes = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines(
        nodes: nodes == _undefined
            ? _instance.nodes
            : (nodes as List<Fragment$PipelineFields?>?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  TRes nodes(
          Iterable<Fragment$PipelineFields?>? Function(
                  Iterable<
                      CopyWith$Fragment$PipelineFields<
                          Fragment$PipelineFields>?>?)
              _fn) =>
      call(
          nodes: _fn(_instance.nodes?.map((e) => e == null
              ? null
              : CopyWith$Fragment$PipelineFields(
                  e,
                  (i) => i,
                )))?.toList());
}

class _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines<
        TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines<
            TRes> {
  _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$pipelines(
      this._res);

  TRes _res;

  call({
    List<Fragment$PipelineFields?>? nodes,
    String? $__typename,
  }) =>
      _res;

  nodes(_fn) => _res;
}

class Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines
    implements
        Fragment$UserProjects$projectMemberships$nodes$project$tagPipelines,
        Fragment$ProjectFields$tagPipelines {
  Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines({
    this.nodes,
    this.$__typename = 'PipelineConnection',
  });

  factory Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines.fromJson(
      Map<String, dynamic> json) {
    final l$nodes = json['nodes'];
    final l$$__typename = json['__typename'];
    return Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines(
      nodes: (l$nodes as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Fragment$PipelineFields.fromJson((e as Map<String, dynamic>)))
          .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final List<Fragment$PipelineFields?>? nodes;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$nodes = nodes;
    _resultData['nodes'] = l$nodes?.map((e) => e?.toJson()).toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$nodes = nodes;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$nodes == null ? null : Object.hashAll(l$nodes.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other
            is Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$nodes = nodes;
    final lOther$nodes = other.nodes;
    if (l$nodes != null && lOther$nodes != null) {
      if (l$nodes.length != lOther$nodes.length) {
        return false;
      }
      for (int i = 0; i < l$nodes.length; i++) {
        final l$nodes$entry = l$nodes[i];
        final lOther$nodes$entry = lOther$nodes[i];
        if (l$nodes$entry != lOther$nodes$entry) {
          return false;
        }
      }
    } else if (l$nodes != lOther$nodes) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines
    on Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines {
  CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines<
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines>
      get copyWith =>
          CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines<
    TRes> {
  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines(
    Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines
        instance,
    TRes Function(
            Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines)
        then,
  ) = _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines;

  factory CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines.stub(
          TRes res) =
      _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines;

  TRes call({
    List<Fragment$PipelineFields?>? nodes,
    String? $__typename,
  });
  TRes nodes(
      Iterable<Fragment$PipelineFields?>? Function(
              Iterable<
                  CopyWith$Fragment$PipelineFields<Fragment$PipelineFields>?>?)
          _fn);
}

class _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines<
        TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines<
            TRes> {
  _CopyWithImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines(
    this._instance,
    this._then,
  );

  final Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines
      _instance;

  final TRes Function(
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines)
      _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? nodes = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(
          Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines(
        nodes: nodes == _undefined
            ? _instance.nodes
            : (nodes as List<Fragment$PipelineFields?>?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  TRes nodes(
          Iterable<Fragment$PipelineFields?>? Function(
                  Iterable<
                      CopyWith$Fragment$PipelineFields<
                          Fragment$PipelineFields>?>?)
              _fn) =>
      call(
          nodes: _fn(_instance.nodes?.map((e) => e == null
              ? null
              : CopyWith$Fragment$PipelineFields(
                  e,
                  (i) => i,
                )))?.toList());
}

class _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines<
        TRes>
    implements
        CopyWith$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines<
            TRes> {
  _CopyWithStubImpl$Query$CurrentUserPipelines$user$projectMemberships$nodes$project$tagPipelines(
      this._res);

  TRes _res;

  call({
    List<Fragment$PipelineFields?>? nodes,
    String? $__typename,
  }) =>
      _res;

  nodes(_fn) => _res;
}
