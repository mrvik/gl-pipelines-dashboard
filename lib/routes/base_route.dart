import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import 'router.dart';

@RoutePage()
class BasePage extends StatelessWidget {
  const BasePage({super.key});

  @override
  Widget build(BuildContext context) => AutoTabsRouter(
        routes: routeList,
        homeIndex: 0,
        builder: (context, child) => child,
        // bottomNavigationBuilder: (context, tabsRouter) => routerNavigationBar(tabsRouter),
      );
}

const routeList = [
  UserReposRoute(),
  GroupsHomeRoute(),
  ConfigRoute(),
];

Widget routerNavigationBar(TabsRouter router) => BottomNavigationBar(
      currentIndex: router.activeIndex,
      onTap: (value) => router.setActiveIndex(value),
      items: const [
        BottomNavigationBarItem(icon: Icon(Icons.person), label: "User projects"),
        BottomNavigationBarItem(icon: Icon(Icons.group), label: "Groups"),
        BottomNavigationBarItem(icon: Icon(Icons.settings), label: "Settings"),
      ],
    );
