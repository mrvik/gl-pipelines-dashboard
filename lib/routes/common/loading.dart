import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:gitlab_dashboard/routes/base_route.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

mixin DefaultLoadingView {
  Widget loadingScreen(BuildContext context) => Center(
          child: Text.rich(
        const TextSpan(text: "Loading data..."),
        style: Theme.of(context).textTheme.displayMedium,
      ));
}

mixin DefaultExceptionView {
  Widget exceptionScreen(BuildContext context, OperationException ex) => Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Error running query", style: Theme.of(context).textTheme.titleLarge),
        if(ex.linkException != null) Text("Link error: ${ex.linkException}"),
        ...ex.graphqlErrors.map((e) => Text("GraphQL error at ${e.path?.join(">") ?? "root"}: ${e.message}")),
      ],
    ),
  );
}

class LoadingBar extends LinearProgressIndicator implements PreferredSizeWidget {
  const LoadingBar({super.key, required this.preferredSize});

  @override
  final Size preferredSize;
}

class GraphScaffold<T> extends StatelessWidget with DefaultLoadingView, DefaultExceptionView {
  const GraphScaffold({super.key, this.appBar, this.refetch, required this.result, required this.builder});

  final PreferredSizeWidget? Function(bool loading)? appBar;
  final Refetch<T>? refetch;
  final QueryResult<T> result;
  final Widget? Function(T? data) builder;

  @override
  Widget build(BuildContext context) {
    final router = AutoTabsRouter.of(context);

    return Scaffold(
      appBar: appBar?.call(result.isLoading),
      body: result.hasException
          ? exceptionScreen(context, result.exception!)
          : result.isLoading && result.parsedData == null
              ? loadingScreen(context)
              : builder(result.parsedData),
      floatingActionButton: refetch != null && !result.isLoading
          ? FloatingActionButton(
              onPressed: refetch,
              tooltip: "Refresh",
              child: const Icon(Icons.refresh),
            )
          : null,
      bottomNavigationBar: routerNavigationBar(router),
    );
  }
}
