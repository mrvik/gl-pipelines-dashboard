import 'package:hive/hive.dart';

class GitLabConfig {
  final String endpoint;
  final String token;

  GitLabConfig({required this.endpoint, required this.token});
}

class GitLabConfigAdapter extends TypeAdapter<GitLabConfig> {
  @override
  GitLabConfig read(BinaryReader reader) {
    final values = reader.readStringList();
    return GitLabConfig(endpoint: values[0], token: values[1]);
  }

  @override
  final typeId = 100;

  @override
  void write(BinaryWriter writer, GitLabConfig obj) => writer.writeStringList([
    obj.endpoint,
    obj.token,
  ]);
}