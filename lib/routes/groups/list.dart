import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:gitlab_dashboard/queries/groups.graphql.dart';
import 'package:gitlab_dashboard/routes/common/loading.dart';
import 'package:gitlab_dashboard/routes/router.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class GroupList extends StatelessWidget {
  const GroupList({super.key, required this.projects});

  final List<Fragment$GroupInfo> projects;

  @override
  Widget build(BuildContext context) => projects.isEmpty
      ? Center(child: Text.rich(TextSpan(text: "No groups to display", style: Theme.of(context).textTheme.displayMedium)))
      : ListView.builder(
          itemCount: projects.length,
          itemBuilder: (context, index) => ListTile(
            title: Text(projects[index].name),
            subtitle: Text(projects[index].fullPath),
            onTap: () => context.router.push(PipelinesRoute(rawGroup: projects[index].fullPath)),
          ),
        );
}

@RoutePage()
class GroupListPage extends StatelessWidget {
  const GroupListPage({super.key});

  @override
  Widget build(BuildContext context) => Query<Query$UserGroups>(
      options: QueryOptions<Query$UserGroups>(
        document: documentNodeQueryUserGroups,
        parserFn: Query$UserGroups.fromJson,
      ),
      builder: (result, {fetchMore, refetch}) => GraphScaffold(
            result: result,
            refetch: refetch,
            appBar: (loading) => AppBar(
              title: const Text("Group list"),
              leading: const AutoLeadingButton(),
              bottom: !loading ? null : const LoadingBar(preferredSize: Size(double.infinity, 6)),
            ),
            builder: (result) => GroupList(
                projects: result?.currentUser?.groups?.nodes?.whereType<Fragment$GroupInfo>().toList() ?? List.empty()),
          ));
}
