import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

@RoutePage()
class GroupsHomePage extends StatelessWidget {
  const GroupsHomePage({super.key});

  @override
  Widget build(BuildContext context) => const AutoRouter();
}