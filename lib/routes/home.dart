import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:gitlab_dashboard/routes/base_route.dart';
import 'package:gitlab_dashboard/routes/common/objects.dart';
import 'package:hive_flutter/hive_flutter.dart';

@RoutePage()
class ConfigPage extends StatefulWidget {
  const ConfigPage({super.key});

  @override
  State<ConfigPage> createState() => _ConfigPageState();
}

class _ConfigPageState extends State<ConfigPage> {
  String? _endpoint;
  String? _token;

  final _box = Hive.box<GitLabConfig>("gitlab-config");

  bool get _valid => (_endpoint?.isNotEmpty ?? false);

  @override
  void initState() {
    super.initState();

    final conf = _box.get("default");
    if (conf == null) return;

    _endpoint = conf.endpoint;
    _token = conf.token;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text("GitLab credentials"),
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: double.infinity,
              width: MediaQuery.of(context).size.width * 0.75,
              child: Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.only(top: 12),
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      label: Text("GitLab endpoint"),
                      border: OutlineInputBorder(),
                    ),
                    onChanged: (value) => setState(() => _endpoint = value),
                    initialValue: _endpoint,
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      label: Text("GitLab token"),
                      border: OutlineInputBorder(),
                    ),
                    obscureText: true,
                    onChanged: (value) => setState(() => _token = value),
                    initialValue: _token,
                  ),
                ]
                    .map((w) => Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8),
                          child: w,
                        ))
                    .toList(growable: false),
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: _valid ? _updateStore(context.router) : null,
          tooltip: "Save",
          child: const Icon(Icons.done),
        ),
    bottomNavigationBar: routerNavigationBar(AutoTabsRouter.of(context)),
      );

  _updateStore(StackRouter router) => () async {
        await _box.put("default", GitLabConfig(endpoint: _endpoint!, token: _token ?? ""));
        router.pushNamed("/groups");
      };
}