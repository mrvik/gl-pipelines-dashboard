import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:gitlab_dashboard/routes/common/loading.dart';
import 'package:gitlab_dashboard/routes/pipelines/project.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../../queries/pipelines.graphql.dart';

@RoutePage()
class PipelinesPage extends StatelessWidget {
  PipelinesPage({super.key, @QueryParam("group") rawGroup = ""})
      : group = Uri.decodeComponent(rawGroup);

  final String group;

  @override
  Widget build(BuildContext context) => group == ""
      ? Center(
          child: Text.rich(TextSpan(
              text: "No group selected to display",
              style: Theme.of(context).textTheme.displayMedium)))
      : Query(
          options: QueryOptions<Query$GroupProjectPipelines>(
            document: documentNodeQueryGroupProjectPipelines,
            variables: {
              "group": group,
            },
            fetchPolicy: FetchPolicy.cacheAndNetwork,
            pollInterval: const Duration(seconds: 10),
            parserFn: Query$GroupProjectPipelines.fromJson,
          ),
          builder: (result, {fetchMore, refetch}) => GraphScaffold(
                result: result,
                refetch: refetch,
                appBar: (loading) => AppBar(
                  leading: const AutoLeadingButton(),
                  title: Text("$group group pipelines"),
                  bottom: !loading
                      ? null
                      : const LoadingBar(
                          preferredSize: Size(double.infinity, 6)),
                ),
                builder: _resultComponent,
              ));

  Widget _listComponent(Query$GroupProjectPipelines data) => ListView.builder(
        itemBuilder: (context, index) => ProjectInfo(
          projectInfo: data.group!.projects.nodes![index]!,
        ),
        itemCount: data.group?.projects.nodes?.length ?? 0,
      );

  Widget _resultComponent(Query$GroupProjectPipelines? result) =>
      _listComponent(result!);
}
