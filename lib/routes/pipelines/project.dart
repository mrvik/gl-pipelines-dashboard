import 'package:flutter/material.dart';
import 'package:gitlab_dashboard/queries/pipelines.graphql.dart';
import 'package:gitlab_dashboard/queries/schema.graphql.dart';

import '../../functional.dart';

class ProjectInfo extends StatelessWidget {
  const ProjectInfo({super.key, required this.projectInfo});

  final Fragment$ProjectFields projectInfo;

  @override
  Widget build(BuildContext context) => Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.baseline,
                textBaseline: TextBaseline.ideographic,
                children: [
                  Flexible(
                    flex: 3,
                    child: Column(
                      children: [
                        ListTile(
                          title: Text(projectInfo.name),
                          subtitle: Text(projectInfo.fullPath),
                        ),
                        ...projectInfo.pipelines!.nodes!
                            .where((n) => n != null)
                            .map((e) => Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                                child: PipelineInfo(pipeline: e!))),
                      ],
                    ),
                  ),
                  maybe(
                      maybeFirst(projectInfo.tagPipelines?.nodes) ?? defaultBranch(projectInfo),
                      (p) => Flexible(
                          flex: 2,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: PipelineInfo(pipeline: p),
                          ))),
                ].whereType<Widget>().toList(growable: false),
              ),
            ],
          ),
        ),
      );
}

T? maybeFirst<T>(Iterable<T>? of) => (of?.isEmpty ?? true) ? null : of!.first;

class PipelineInfo extends StatelessWidget {
  const PipelineInfo({super.key, required this.pipeline});

  static final _icons = {
    "status_success": Icons.done,
    "status_failed": Icons.error,
    "status_skipped": Icons.fast_forward,
    "status_running": Icons.build,
    "status_pending": Icons.pending,
    "status_warning": Icons.warning,
  };

  static final _statusColors = {
    "status_success": Colors.lightGreen,
    "status_failed": Colors.red,
    "status_skipped": Colors.black26,
    "status_running": Colors.lightBlue,
    "status_pending": Colors.yellow,
    "status_warning": Colors.deepOrange,
  };

  final Fragment$PipelineFields pipeline;

  @override
  Widget build(BuildContext context) => Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(_icons[pipeline.detailedStatus.icon!] ?? Icons.question_mark),
          Padding(
            padding: const EdgeInsets.only(left: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const Text("Pipeline for "),
                    Text("${pipeline.ref!}: ${pipeline.detailedStatus.text!} at ${_date(pipeline.pipelineDate)}"),
                  ],
                ),
                const Divider(
                  height: 2,
                ),
                maybe(pipeline.commit?.title?.trim(), (s) => subtitle(context, s)),
                const Divider(
                  height: 4,
                ),
                Row(
                  children: [
                    ...pipeline.stages?.nodes?.where((e) => e != null).map((e) => _jobElement(context, e!)) ??
                        const Iterable.empty(),
                  ],
                )
              ].whereType<Widget>().toList(growable: false),
            ),
          ),
        ],
      );

  Widget _jobElement(BuildContext ctx, Fragment$PipelineFields$stages$nodes e) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: _statusColor(e.detailedStatus!.icon!).withOpacity(0.4),
            border: Border.all(
              color: _statusColor(e.detailedStatus!.icon!),
              width: 2,
            ),
            borderRadius: const BorderRadiusDirectional.only(
              topEnd: Radius.circular(20),
              bottomStart: Radius.circular(20),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(e.name!),
                Text(
                    " ${e.jobs?.nodes?.where((element) => element?.status == Enum$CiJobStatus.SUCCESS).length ?? 0}/${e.jobs?.nodes?.length ?? 0}"),
                const Text(": "),
                if (e.detailedStatus?.icon != null) Icon(_icons[e.detailedStatus!.icon]) else Text(e.status!),
              ],
            ),
          ),
        ),
      );

  Color _statusColor(String iconName) => _statusColors[iconName] ?? Colors.grey;

  String _date(String from) {
    final parsed = DateTime.parse(from).toLocal();
    return "${parsed.year}/${_padZero(parsed.month)}/${_padZero(parsed.day)} ${_padZero(parsed.hour)}:${_padZero(parsed.minute)}";
  }

  String _padZero(int number) => number.toString().padLeft(2, '0');
}

Widget subtitle(BuildContext context, String msg) => Text.rich(
      TextSpan(text: msg),
      style: Theme.of(context).textTheme.bodySmall,
    );

Fragment$PipelineFields? defaultBranch(Fragment$ProjectFields from) {
  final branchName = from.repository?.defaultBranch ?? "main";
  final pipeline = from.pipelines?.nodes?.lastWhere((element) => element?.ref == branchName, orElse: () => null);

  return pipeline;
}
