import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:gitlab_dashboard/routes/groups/list.dart';
import 'package:gitlab_dashboard/routes/groups/main.dart';
import 'package:gitlab_dashboard/routes/home.dart';
import 'package:gitlab_dashboard/routes/pipelines/main.dart';
import 'package:gitlab_dashboard/routes/users/user_repos.dart';

import 'base_route.dart';

part 'router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
    AutoRoute(
      path: "/",
      page: BaseRoute.page,
      children: [
        RedirectRoute(path: "", redirectTo: "config"),
        AutoRoute(
          path: "config",
          page: ConfigRoute.page,
        ),
        AutoRoute(
          path: "user",
          page: UserReposRoute.page,
        ),
        AutoRoute(
          path: "groups",
          page: GroupsHomeRoute.page,
          children: [
            AutoRoute(
              path: "",
              page: GroupListRoute.page,
              fullMatch: true,
              usesPathAsKey: true,
            ),
            AutoRoute(
              path: "pipelines",
              page: PipelinesRoute.page,
              fullMatch: true,
              usesPathAsKey: true,
              maintainState: false,
            ),
          ],
        ),
        RedirectRoute(path: "*", redirectTo: "/"),
      ],
    ),
  ];
}
