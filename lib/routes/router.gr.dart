// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    BaseRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const BasePage(),
      );
    },
    ConfigRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const ConfigPage(),
      );
    },
    GroupListRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const GroupListPage(),
      );
    },
    GroupsHomeRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const GroupsHomePage(),
      );
    },
    PipelinesRoute.name: (routeData) {
      final queryParams = routeData.queryParams;
      final args = routeData.argsAs<PipelinesRouteArgs>(
          orElse: () => PipelinesRouteArgs(
                  rawGroup: queryParams.get(
                'group',
                "",
              )));
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: PipelinesPage(
          key: args.key,
          rawGroup: args.rawGroup,
        ),
      );
    },
    UserReposRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const UserReposPage(),
      );
    },
  };
}

/// generated route for
/// [BasePage]
class BaseRoute extends PageRouteInfo<void> {
  const BaseRoute({List<PageRouteInfo>? children})
      : super(
          BaseRoute.name,
          initialChildren: children,
        );

  static const String name = 'BaseRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [ConfigPage]
class ConfigRoute extends PageRouteInfo<void> {
  const ConfigRoute({List<PageRouteInfo>? children})
      : super(
          ConfigRoute.name,
          initialChildren: children,
        );

  static const String name = 'ConfigRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [GroupListPage]
class GroupListRoute extends PageRouteInfo<void> {
  const GroupListRoute({List<PageRouteInfo>? children})
      : super(
          GroupListRoute.name,
          initialChildren: children,
        );

  static const String name = 'GroupListRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [GroupsHomePage]
class GroupsHomeRoute extends PageRouteInfo<void> {
  const GroupsHomeRoute({List<PageRouteInfo>? children})
      : super(
          GroupsHomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'GroupsHomeRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [PipelinesPage]
class PipelinesRoute extends PageRouteInfo<PipelinesRouteArgs> {
  PipelinesRoute({
    Key? key,
    dynamic rawGroup = "",
    List<PageRouteInfo>? children,
  }) : super(
          PipelinesRoute.name,
          args: PipelinesRouteArgs(
            key: key,
            rawGroup: rawGroup,
          ),
          rawQueryParams: {'group': rawGroup},
          initialChildren: children,
        );

  static const String name = 'PipelinesRoute';

  static const PageInfo<PipelinesRouteArgs> page =
      PageInfo<PipelinesRouteArgs>(name);
}

class PipelinesRouteArgs {
  const PipelinesRouteArgs({
    this.key,
    this.rawGroup = "",
  });

  final Key? key;

  final dynamic rawGroup;

  @override
  String toString() {
    return 'PipelinesRouteArgs{key: $key, rawGroup: $rawGroup}';
  }
}

/// generated route for
/// [UserReposPage]
class UserReposRoute extends PageRouteInfo<void> {
  const UserReposRoute({List<PageRouteInfo>? children})
      : super(
          UserReposRoute.name,
          initialChildren: children,
        );

  static const String name = 'UserReposRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}
