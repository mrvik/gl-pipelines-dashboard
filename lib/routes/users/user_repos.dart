import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:gitlab_dashboard/queries/pipelines.graphql.dart';
import 'package:gitlab_dashboard/routes/common/loading.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../pipelines/project.dart';

@RoutePage()
class UserReposPage extends StatelessWidget {
  const UserReposPage({super.key});

  @override
  Widget build(BuildContext context) => Query<Query$CurrentUserPipelines>(
        options: QueryOptions<Query$CurrentUserPipelines>(
          document: documentNodeQueryCurrentUserPipelines,
          pollInterval: const Duration(seconds: 20),
          fetchPolicy: FetchPolicy.cacheAndNetwork,
          parserFn: Query$CurrentUserPipelines.fromJson,
        ),
        builder: (result, {fetchMore, refetch}) => GraphScaffold(
          result: result,
          refetch: refetch,
          appBar: (loading) => AppBar(
            title: const Text("Pipelines for current user's projects"),
            bottom: !loading ? null : const LoadingBar(preferredSize: Size(double.infinity, 6)),
          ),
          builder: (data) => _resultComponent(data!),
        ),
      );

  Widget _resultComponent(Query$CurrentUserPipelines data) {
    final projects = data.user?.projectMemberships?.nodes
            ?.where((element) => element?.project?.group == null && element?.project?.namespace?.fullPath == data.user?.username)
            .where((element) => (element?.project?.pipelines?.nodes?.length ?? 0) > 0)
            .toList(growable: false) ??
        [];

    return ListView.builder(
      itemBuilder: (context, index) => ProjectInfo(
        projectInfo: projects[index]!.project!,
      ),
      itemCount: projects.length,
    );
  }
}
